

class AppConstants {

    static get RESTAURANTS() {
        return 'restaurants';
    }

    static get ORDER_REVIEW_COLLECTION() {
        return "Reviews";
    }


}

export default AppConstants;