import { RestaurantItem } from '../models/restaurant_menu';

class Order {

    /**
     * 
     * @param {string} id 
     * @param {string} customerId 
     * @param {Array<RestaurantItem>} itemOrders 
     * @param {string} bookingType 
     * @param {number} statusCode 
     * @param {string} restaurantId 
     * @param {Date} recordedAt 
     * @param {string} paymentMethod 
     * @param {number} tableNumber 
     */
    constructor(id, customerId, itemOrders, bookingType, statusCode, restaurantId, recordedAt, paymentMethod, tableId, cancelReason, ) {
        this.id = id;
        this.customerId = customerId;
        this.itemOrders = itemOrders;
        this.bookingType = bookingType;
        this.statusCode = statusCode;
        this.restaurantId = restaurantId;
        this.recordedAt = recordedAt;
        this.paymentMethod = paymentMethod;
        this.tableId = tableId;
        this.cancelReason = cancelReason;
    }

    static fromJson(json) {
        return new Order(
            json["id"],
            json["customerId"],
            (json["itemOrders"] || []).map((val) => RestaurantItem.fromJson(val)),
            json["bookingType"],
            json["statusCode"],
            json["restaurantId"],
            json["recordedAt"],
            json["paymentMethod"],
            json["tableId"],
            json["cancelReason"] || '',
            json["totalCost"] || 0
        );
    }

    getItemCount = (itemId) => {
        return (this.itemOrders || []).filter((val) => val.id == itemId).length;
    }

    /**
     * 
     * @param {Order} order 
     */
    static totalCost(order) {
        let totalCost = 0;
        order.itemOrders.forEach((val) => {
            totalCost += parseInt(val.price || 0);
            console.log(val);
            (val.customizationsApplied || []).forEach((customization) => {
                totalCost += parseInt(customization.price || 0);
            })
        });
        return totalCost;
    }

    get name() {
        let arr = Array.from(new Set(this.itemOrders || []).values());
        let name = arr.map((val) => val.name).join(", ");
        console.log(name)
        return `${name}`;
    }
}

export default Order;