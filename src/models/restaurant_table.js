export default class RestaurantTable {
    constructor(id, number, restaurantId, orderId, isOccupied) {
        this.id = id;
        this.number = number;
        this.restaurantId = restaurantId;
        this.orderId = orderId;
        this.isOccupied = isOccupied;
    }

    static fromJson(json) {
        return new RestaurantTable(
            json["id"],
            json["number"],
            json['restaurantId'],
            json["orderId"],
            json["isOccupied"]
        );
    }

}
