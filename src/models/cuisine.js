
class Cuisine {

    /**
     * 
     * @param {string} id 
     * @param {string} name 
     */
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}