import { Cuisine } from '../models/restaurant_menu';

class Restaurant {

    /**
     * 
     * @param {string} id 
     * @param {string} name 
     * @param {string} description 
     * @param {string} thumb 
     * @param {Array<string>} photos 
     * @param {boolean} isCashAllowed 
     * @param {RestaurantLocation} location 
     * @param {string} phoneNumber 
     * 
     */
    constructor(id, name, description, thumb, photos, averageCostForTwo, rating, isCashAllowed, location, phoneNumber, cuisineIds, votesCount, currency) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.thumb = thumb;
        this.photos = photos;
        this.averageCostForTwo = averageCostForTwo;
        this.rating = rating;
        this.isCashAllowed = isCashAllowed;
        this.location = location;
        this.phoneNumber = phoneNumber;
        this.cuisineIds = cuisineIds;
        this.votesCount = votesCount;
        this.currency = currency;
    }

    static fromJson(json) {
        return new Restaurant(
            json["id"],
            json["name"],
            json["description"],
            json["thumb"],
            json["photos"],
            json["average_cost_for_two"],
            json["rating"] || 0,
            json["isCashAllowed"],
            json["location"],
            json["phoneNumber"],
            json["cuisineIds"] || [],
            json["votesCount"] || 0,
            json["currency"],
        );
    }

}

class RestaurantLocation {

    /**
     * 
     * @param {string} placeId 
     * @param {string} address 
     * @param {number} longitude 
     * @param {number} latitude 
     * @param {number} zipCode 
     * @param {string} city 
     */
    constructor(placeId, address, longitude, latitude, zipCode, city) {
        this.placeId = placeId;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
        this.zipCode = zipCode;
        this.city = city;
    }

}

class RestaurantReview {

    /**
     * 
     * @param {string} id 
     * @param {string} restaurantId 
     * @param {string} customerId 
     * @param {string} customerName 
     * @param {string} customerAvatar 
     * @param {string} orderId 
     * @param {Date} recordedAt 
     * @param {string} description 
     * @param {number} rating 
     */
    constructor(id, restaurantId, customerId, customerName, customerAvatar, orderId, recordedAt, description, rating) {
        this.id = id;
        this.restaurantId = restaurantId;
        this.customerId = customerId;
        this.customerName = customerName;
        this.customerAvatar = customerAvatar;
        this.orderId = orderId;
        this.recordedAt = recordedAt;
        this.description = description;
        this.rating = rating;
    }

}

class RestaurantFilter {

    /**
     * 
     * @param {Array<Cuisine>} cuisines 
     * @param {RestaurantLocation} location 
     * @param {Array<string>} keys 
     */
    constructor(cuisines, location, keys) {
        this.cuisines = cuisines;
        this.location = location;
        this.keys = keys;
    }

}

const FilterKey = {
    PRICE: "price",
    AVERAGE_COST_FOR_TWO: "average_cost_for_two",
    CASH_ALLOWED: "isCashAllowed",
};

export { RestaurantLocation, RestaurantReview, RestaurantFilter };
export default Restaurant;