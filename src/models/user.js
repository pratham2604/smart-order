import AppConstants from "../helpers/app_constants";



class User {

    /**
     * 
     * @param {string} id 
     * @param {string} name 
     * @param {string} email 
     * @param {string} mobileNumber 
     * @param {string} googleId 
     * @param {string} facebookId 
     */
    constructor(id, name, email, mobileNumber, googleId, facebookId) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.googleId = googleId;
        this.facebookId = facebookId;
    }

    static fromJson(json) {
        return new User(
            json["id"],
            json['name'],
            json['email'],
            json["mobileNumber"],
            json["googleId"],
            json["facebookId"],
        );
    }
}

export default User;