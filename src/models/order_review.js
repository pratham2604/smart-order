import User from '../models/user';

export default class OrderReview {

    /**
     * 
     * @param {string} id 
     * @param {string} restaurantId 
     * @param {string} orderId 
     * @param {string} customerId 
     * @param {double} rating 
     * @param {string} description 
     * @param {User} user 
     * @param {Date} recordedAt 
     */
    constructor(id, restaurantId, orderId, customerId, rating, description, user, recordedAt) {
        this.id = id;
        this.restaurantId = restaurantId;
        this.orderId = orderId;
        this.customerId = customerId;
        this.rating = rating;
        this.description = description;
        this.user = user;
        this.recordedAt = recordedAt
    }


    static fromJson(json) {
        return new OrderReview(
            json["id"],
            json["restaurantId"],
            json["orderId"],
            json["customerId"],
            json["rating"],
            json["description"],
            User.fromJson(json["user"]),
            json["recordedAt"]
        );
    }


}