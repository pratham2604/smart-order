

class RestaurantMenu {

    /**
     * 
     * @param {string} id 
     * @param {string} restaurantId 
     * @param {string} items 
     */
    constructor(id, restaurantId, items, ) {
        this.id = id;
        this.restaurantId = restaurantId;
        this.items = items;
    }

    static fromJson(json) {
        return new RestaurantMenu(
            json["id"],
            json["restaurantId"],
            (json["items"] || []).map((val) => {
                val["restaurantId"] = json["restaurantId"];
                return RestaurantItem.fromJson(val);
            }),
        );
    }

}


class RestaurantItem {

    /**
     * 
     * @param {string} id 
     * @param {string} name 
     * @param {string} ingredient 
     * @param {string} thumbnail 
     * @param {Cuisine} cuisine 
     * @param {boolean} isAvailable 
     * @param {string} type
     * @param {Array<RestaurantItemCustomization>} availableCustomizations 
     * @param {string} restaurantId 
     * @param {Array<{name,price}>}customizationsApplied
     */
    constructor(id, name, ingredient, thumbnail, price, type, cuisine, isAvailable, customizations, restaurantId, allowChefNote) {
        this.id = id;
        this.name = name;
        this.ingredient = ingredient;
        this.thumbnail = thumbnail;
        this.price = price;
        this.type = type;
        this.cuisine = cuisine;
        this.isAvailable = isAvailable;
        this.customizations = customizations || [];
        this.restaurantId = restaurantId;
        this.allowChefNote = allowChefNote;
    }

    static fromJson(json) {
        return new RestaurantItem(
            json["id"],
            json["name"],
            json["ingredient"],
            json["thumbnail"],
            json["price"],
            json["type"],
            Cuisine.fromJson(json["cuisine"] || {}),
            json["isAvailable"],
            json["customizations"],
            json["restaurantId"],
            json["allowChefNote"]
        );
    }


}

class RestaurantItemCustomization {

    constructor(id, title, isMultiple, isRequired, options) {
        this.id = id;
        this.title = title;
        this.isMultiple = isMultiple;
        this.isRequired = isRequired;
        this.customizations = options;
    }

    static fromJson(json) {
        return new RestaurantItemCustomization(
            json["id"],
            json["title"],
            json["isMultiple"] || false,
            json["isRequired"] || false,
            json["customizations"]
        );
    }
}


class Cuisine {

    constructor(id, name, image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    static fromJson(json) {
        return new Cuisine(json["id"], json["name"], json["image"]);
    }

}

export { RestaurantMenu, RestaurantItem, RestaurantItemCustomization, Cuisine };