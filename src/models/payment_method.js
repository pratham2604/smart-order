

class PaymentMethod {

    static get CASH() {
        return "Cash";
    };

    static get ONLINE() {
        return "Online";
    }

    constructor(id, title, description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

}

export default PaymentMethod;
