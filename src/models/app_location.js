

class AppLocation {

    /**
     * 
     * @param {string} title 
     * @param {string} description 
     * @param {string} placeId 
     * @param {number} lat
     * @param {number} lng
     */
    constructor(title, description, placeId, lat, lng) {
        this.title = title;
        this.description = description;
        this.placeId = placeId;
        this.lat = lat;
        this.lng = lng;
    }

    static fromJson(json) {
        return new AppLocation(
            json["structured_formatting"]["main_text"],
            json["description"],
            json["place_id"]
        );
    }
}

export default AppLocation;