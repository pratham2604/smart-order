

class PaymentTransaction {

    /**
     * 
     * @param {string} id 
     * @param {string} customerId 
     * @param {string} orderId 
     * @param {string} paymentId 
     * @param {string} restaurantId 
     * @param {string} totalAmount 
     * @param {string} recordedAt
     */
    constructor(id, customerId, orderId, paymentId, restaurantId, totalAmount, recordedAt, method) {
        this.id = id;
        this.customerId = customerId;
        this.orderId = orderId;
        this.paymentId = paymentId;
        this.restaurantId = restaurantId;
        this.totalAmount = totalAmount;
        this.recordedAt = recordedAt;
        this.method = method;
    }

    static fromJson(json) {
        return new PaymentTransaction(
            json["id"],
            json["customerId"],
            json["orderId"],
            json["paymentId"],
            json["restaurantId"],
            json["totalAmount"],
            json["recordedAt"],
            json["method"]
        );
    }

}

export default PaymentTransaction;
