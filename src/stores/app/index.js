import { createStore } from 'redux';
import { appReducer } from '../../reducers/app';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

const persistConfig = {
    key: 'root',
    storage: storage,
};

const pReducer = persistReducer(persistConfig, appReducer);

export const appStore = createStore(pReducer);
export const persistor = persistStore(appStore);