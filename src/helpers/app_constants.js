

class AppConstants {

    static get REVIEW_COLLECTION() {
        return "reviews";
    }

    static getURL(url) {
        return "https://cors-anywhere.herokuapp.com/" + url;
    }

    static get RESTAURANTS() {
        return "restaurants";
    }

    static get ORDER_COLLECTION() {
        return "orders";
    }

    static get CANCEL_REQUESTS_COLLECTION() {
        return "cancel_requests";
    }

    static get RESTAURANT_MENU() {
        return "restaurant_menu";
    }

    static get ABOUT_US() {
        return "About Us";
    }

    static get PAYMENTS() {
        return "payments";
    }

    static get ORDERS() {
        return "orders";
    }

    static get CUISINES() {
        return "cuisines";
    }
    static get DINE_IN() {
        return "dine_in";
    }
    static get ORDER_PICKUP() {
        return "order_pickup";
    }

    static get LOGIN() {
        return "Login";
    }

    static get SIGN_IN_DATA() {
        return "Sign in to access your details";
    }

    static get LOGIN_WITH_FACEBOOK() {
        return "Login with Facebook";
    }

    static get LOGIN_WITH_GOOGLE() {
        return "Login with Google";
    }

    static get LOGIN_WITH_MOBILE() {
        return "Login with Mobile";
    }

    static get MOBILE_NUMBER() {
        return "Mobile Number";
    }

    static get OTP() {
        return "OTP"
    }

    static get POPULAR() {
        return "Popular"
    }

    // User

    static get USER_COLLECTION() {
        return "users";
    }

    static get USER_MOBILE() {
        return "mobileNumber";
    }

    static get CONFIRMED() {
        return 0;
    }

    static get DELIVERED() {
        return 1;
    }

    static get NOT_CONFIRMED() {
        return -2;
    }

    static get CANCELLED() {
        return -1;
    }

    static get IN_KITCHEN() {
        return -1;
    }

    static get SEARCH_RESTAURANT() {
        return "Search for restaurants by name.";
    }

    static get DISCOVER() {
        return "Discover";
    }

    static get RESTAURANTS_UPERCASE() {
        return "Restaurants";
    }

    static get THAT() {
        return "That";
    }

    static get DELIVERS_NEAR_YOU() {
        return "delivers near you";
    }

    static get RESTAURANT_PATH() {
        return "/restaurant";
    }


    static get RESTAURANTS_PATH() {
        return "/restaurants";
    }

    static get CUISINE_PATH() {
        return "/cuisine";
    }

    static get CHECKOUT_PATH() {
        return "/checkout";
    }

    static get RESTAURANT_TABLE_COLLECTION() {
        return "restaurant_table";
    }

    // Links
    static getSearchLink(query) {
        return `${AppConstants.RESTAURANTS}?query=${query}`;
    }

    static getRestaurantByCuisineLink(cuisine) {
        return `${AppConstants.RESTAURANTS}?cuisine=${cuisine}`;
    }
    static getRestaurantPath(restaurantId) {
        return `${AppConstants.RESTAURANT_PATH}?id=${restaurantId}`;
    }
    static getRestaurantWithTablePath(restaurantId, tableId) {
        return `${AppConstants.RESTAURANT_PATH}?id=${restaurantId}&tableId=${tableId}`;
    }

    static get FIND_FOOD() {
        return "Find Food";
    }


}

export default AppConstants;