import React, { Component } from 'react';
import RestaurantComponent from './RestaurantComponent';
import RestaurantFilterComponent from './RestaurantFilterComponent';
import RestaurantController from '../../controllers/restaurant_controller';
import AppliedFilter from '../../models/applied_filter';
import GridLoadingShimmer from '../common/GridLoadingShimmer';
import Swiper from 'react-id-swiper';
import './style.css';
import 'swiper/css/swiper.css'
import { Empty } from 'antd';
const queryString = require('query-string');

class RestaurantList extends Component {

    state = {
        loading: true,
        restaurants: [],
        restaurantFilter: null,
        filteredRestaurants: [],
        cuisine: null,
        query: null,
        appliedFilter: new AppliedFilter(),
        restaurantsByCuisine: {}
    }

    async componentDidMount() {
        const appliedFilter = { ...this.state };
        const parsed = queryString.parse(window.location.search);
        // Finding restaurants by cuisine, query and fooditems
        if (parsed) {
            if (parsed['cuisine']) {
                let cuisine = parsed['cuisine'].toString();
                appliedFilter.cuisine = cuisine;
                this.setState({
                    appliedFilter
                });
            }
            if (parsed["query"]) {
                let query = parsed['query'].toString();
                appliedFilter.query = query;
                this.setState({
                    appliedFilter
                });
            }
        }
        // 
        this.setState({
            loading: true
        });
        await Promise.all([
            this.loadRestaurants(appliedFilter),
            this.loadRestaurantFilter()
        ]);

        this.setState({
            loading: false
        })
    }

    // Load Restaurants
    loadRestaurants = async (appliedFilter) => {
        const { query, cuisine } = (appliedFilter || this.state);
        let result = [];
        if (query) {
            result = await RestaurantController.searchRestaurants(query || '')
        }
        else if (cuisine) {
            result = await RestaurantController.getRestaurantsByCuisine(cuisine || '');
        }
        else {
            result = await RestaurantController.searchRestaurants('')
        }
        this.setState({
            restaurants: result,
            filteredRestaurants: result

        })
    }

    // Load Restaurant 
    loadRestaurantFilter = async () => {
        let restaurantFilter = await RestaurantController.getRestaurantFilter();
        this.setState({
            restaurantFilter: restaurantFilter,
        });
    }

    // On appliedFilter applied
    onFilterApplied = async (newFilterApplied) => {
        const { restaurants } = this.state;
        const key = newFilterApplied.key;
        let filtered = restaurants.sort((a, b) => {
            if ((typeof a[key]) == "number" && (typeof b[key] == "number")) {
                return a[key] - b[key];
            }
            return `${a[key]}`.localeCompare(`${b[key]}`);
        });
        let cuisine = newFilterApplied.cuisine;
        if (cuisine) {
            filtered = filtered.filter((restaurant) => restaurant.cuisineIds.indexOf(cuisine) != -1)
        }
        this.setState({
            appliedFilter: newFilterApplied,
            filteredRestaurants: filtered
        })
    }

    render() {
        let { filteredRestaurants, restaurantFilter, loading, appliedFilter } = this.state;

        const params = {
            slidesPerView: 'auto',
            spaceBetween: 30,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
        }

        console.log(restaurantFilter, filteredRestaurants)
        return (
            loading ?
                <GridLoadingShimmer /> :
                <div className="restaurant-list-body">
                    <h1 style={{ fontWeight: "bold", padding: '1rem 4rem', fontSize: '2.4rem' }}>
                        Popular Restaurant
                    </h1>
                    <div className="restaurant-list-part">
                        <div className="restaurant-list">
                            {filteredRestaurants.length == 0 ?
                                <Empty /> :
                                <Swiper {...params}>
                                    {filteredRestaurants.map((restaurant, index) => {
                                        return (
                                            <div style={{maxWidth: '12rem', display: 'inline-block'}}>
                                                <RestaurantComponent key={restaurant.id} restaurant={restaurant} />
                                            </div>
                                        )
                                    })}
                                </Swiper>
                            }
                        </div>
                    </div>
                </div>
        );
    }
}

export default RestaurantList;