import React, { Component } from 'react';
import { Checkbox, Radio, Button } from 'antd';
import { Link } from 'react-router-dom';
import AppConstants from '../../helpers/app_constants';
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css'

class RestaurantFilterComponent extends Component {
    render() {
        const params = {
            slidesPerView: 'auto',
            spaceBetween: 30,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
        }
        const { restaurantFilter, appliedFilter, onFilterApplied } = this.props;
        const { cuisines, keys } = restaurantFilter;
        console.log(cuisines);
        console.log(appliedFilter)
        return (
            <div className="restaurant-filter">
                <h1>
                    Popular Restaurant
                </h1>
                <br />
                <h3>
                    Cuisines
                </h3>
                <div>
                    <Swiper {...params}>
                    {cuisines.map((cuisine) => {
                        console.log(appliedFilter.cuisine == cuisine.id)
                        return (<Link to={AppConstants.getRestaurantByCuisineLink(cuisine.id)}><Button
                            key={cuisine.name} type="link"
                            onClick={() => {
                                let filter = { ...appliedFilter };
                                filter.cuisine = cuisine.id;
                                onFilterApplied(filter);
                            }}
                            className={"cuisine " + (appliedFilter.cuisine == cuisine.id ? "active" : "")}>{cuisine.name}</Button></Link>)
                    }
                    )}
                    </Swiper>
                </div>
            </div >
        );
    }
}

export default RestaurantFilterComponent;