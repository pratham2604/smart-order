import React, { Component } from 'react';
import { Card, Icon, Avatar, Button, Typography } from 'antd';
import { Link } from 'react-router-dom';
import RestaurantController from '../../controllers/restaurant_controller';

const { Meta } = Card;

const { Text } = Typography;

class RestaurantComponent extends Component {
    render() {
        let { restaurant } = this.props;
        return (
            <Link to={RestaurantController.getRestaurantLink(restaurant.id)} className="restaurant-card-wrapper">
                <div className="restaurant-card">
                    <div className="restaurant-card-img-container">
                        <img alt={restaurant.name} src={restaurant.thumb} />
                    </div>
                    <div className="restaurant-card-body-container">
                        <div className='restaurant-card-body'>
                            <Text className="title" strong>{restaurant.name}</Text>
                            <Text className="subtitle" ellipsis>{restaurant.description}</Text>
                            <div className="details">
                                <span className={"rating"}><Icon type="star" theme="filled" /> {restaurant.rating}</span>
                                <span>•</span>
                                <span>₹ {restaurant.averageCostForTwo} FOR TWO</span>
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        );
    }
}

export default RestaurantComponent;