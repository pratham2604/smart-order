import React, { Component } from 'react';
import SocialButton from './SocialButton';
import { Typography, Button, Modal, Input, Form, message, Spin } from 'antd';
import AppConstants from '../../helpers/app_constants';
import OTPController from '../../controllers/otp_controller';
import UserController from '../../controllers/user_controller';
import { loginUser } from '../../actions/app';
import { connect } from 'react-redux';
import FacebookLoginBtn from './FacebookLoginBtn';
import User from '../../models/user';

const { Title, Text } = Typography;

class MobileOTPDialog extends Component {

    state = {
        name: null,
        mobileNumber: null,
        otp: null,
        isSendingOTP: false,
        isVerifyingOTP: false,
        canVerify: false
    }

    sendOTP = async () => {

        const { mobileNumber } = this.state;
        this.setState({
            isSendingOTP: true
        })

        let error = await OTPController.sendOTP(mobileNumber);
        if (error) {
            message.error(error)
        }
        this.setState({
            isSendingOTP: false,
            canVerify: error == undefined
        })
    }

    verifyOTP = async () => {

        const { otp, mobileNumber, name } = this.state;
        const { onConfirmed } = this.props;
        if (!name || !mobileNumber) {
            message.error("Please enter all fields");
            return;
        }
        this.setState({
            isVerifyingOTP: true
        })
        let error = await OTPController.verifyOTP(mobileNumber, otp);
        this.setState({
            isVerifyingOTP: false,
        })

        if (error) {
            message.error(error)
            return;
        }
        onConfirmed({ name: name, mobileNumber: mobileNumber });
    }

    render() {
        const { visible, onConfirmed } = this.props;
        const { isSendingOTP, isVerifyingOTP, canVerify } = this.state;
        return (
            <Modal title={AppConstants.LOGIN_WITH_MOBILE} visible={visible} destroyOnClose={true} onOk={() => onConfirmed()} onCancel={() => onConfirmed(null)} footer={[<Button type="primary" loading={isVerifyingOTP} onClick={() => this.verifyOTP()}>Verify</Button>]}>
                <Form>
                    <Form.Item label={"Name"}>
                        <Input size="large" onChange={(e) => this.setState({ "name": e.target.value })}
                            name={"Name"}
                        />
                    </Form.Item>
                    <Form.Item label={AppConstants.MOBILE_NUMBER}>
                        <Input size="large" onChange={(e) => this.setState({ "mobileNumber": e.target.value })}
                            name={AppConstants.MOBILE_NUMBER}
                            addonAfter={<Button type="link" onClick={this.sendOTP} loading={isSendingOTP} >Send OTP</Button>} />
                    </Form.Item>
                    <Form.Item label={AppConstants.OTP}>
                        <Input size="large" onChange={(e) => this.setState({ otp: e.target.value })} name={AppConstants.OTP} disabled={!canVerify} />
                    </Form.Item>
                </Form>
            </Modal>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        loginUser: (user) => {
            return dispatch(loginUser(user));
        }
    };
}

class LoginPage extends Component {

    state = {
        showMobileLogin: false,
        showLoadingDialog: false
    }

    // Login With Facebook
    loginWithFacebook = async (result) => {
        let { name, email, id } = result;
        const { loginUser } = this.props
        let user = new User(null, name, email, null, null, id);
        console.log(user)
        this.setState({
            showLoadingDialog: true
        })
        let newUser = await UserController.loginWithFacebook(user);
        this.setState({
            showLoadingDialog: false
        })
        console.log(newUser)
        if (newUser)
            loginUser(newUser);
    }

    // Login with mobile

    loginWithMobile = async (result) => {
        console.log(result)
        if (!result || !result["mobileNumber"] || !result["name"]) {
            message.error("Login with otp failed");
            return;
        }
        this.setState({
            showMobileLogin: false
        })
        const { loginUser } = this.props;
        this.setState({
            showLoadingDialog: true
        })
        let user = await UserController.loginWithMobile(result);
        this.setState({
            showLoadingDialog: false
        })
        if (user.id)
            loginUser(user);
    }


    render() {
        const { showMobileLogin, showLoadingDialog } = this.state;
        return (
            <div>
                <div className="login-page">
                    <Title level={1}>{AppConstants.LOGIN}</Title>
                    <Text style={{ fontSize: "1.1rem" }}>{AppConstants.SIGN_IN_DATA}</Text>
                    <div className="login-button-list">
                        <SocialButton
                            provider={"facebook"}
                            style={{ backgroundColor: "#3b5998", color: "#ffffff" }}
                            appId='484301692225465'
                            onLoginSuccess={(val) => {
                                let profile = val["_profile"];
                                this.loginWithFacebook(profile);
                            }}
                            onLoginFailure={(val) => console.log(val)}
                        >
                            <img src={"/images/facebook.png"} />
                            <span className="text">{AppConstants.LOGIN_WITH_FACEBOOK}
                            </span>
                        </SocialButton>
                        <SocialButton
                            provider={"google"}
                            style={{ backgroundColor: "#ffffff", color: "#000" }}
                            appId='894064438622'
                            onLoginSuccess={(val) => {

                            }}
                            onLoginFailure={(val) => console.log(val)}
                        >
                            <img src={"/images/google.png"} />
                            <span className="text">{AppConstants.LOGIN_WITH_GOOGLE}
                            </span>
                        </SocialButton>


                        <Button className="social-button" onClick={() => this.setState({
                            showMobileLogin: true
                        })}>
                            <img src={"/images/mobile.png"} />
                            <span className="text">{AppConstants.LOGIN_WITH_MOBILE}
                            </span>
                        </Button>
                    </div>
                    <MobileOTPDialog visible={showMobileLogin} onConfirmed={(result) => this.loginWithMobile(result)} />
                </div>
                <Modal title={"Logging in"} destroyOnClose={true} visible={showLoadingDialog} closable={false} footer={[]}><Spin size="large" tip="Loading" /></Modal>

            </div>
        );
    }

}

export default connect(null, mapDispatchToProps)(LoginPage);