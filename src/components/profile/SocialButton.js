import React, { Component } from 'react';
import SocialLogin from 'react-social-login'
import { Button } from 'antd';
import './style.css';

class SocialButton extends Component {
    render() {
        return (
            <Button className="social-button" onClick={this.props.triggerLogin} {...this.props}>
                {this.props.children}
            </Button>
        );
    }
}

export default SocialLogin(SocialButton);