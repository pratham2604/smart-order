import React, { Component } from 'react';
import { List, Button, Menu, Icon, Row, Col, Spin, Typography, Modal, } from 'antd';
import './style.css'
import SubMenu from 'antd/lib/menu/SubMenu';
import PaymentController from '../../controllers/payment_controller';
import OrderController from '../../controllers/order_controller';
import { logoutUser } from '../../actions/app';
import { connect } from 'react-redux';
import Order from '../../models/order';
import OrderComponent from '../order/OrderComponent';

const { Text } = Typography;

const mapDispatchToProps = (dispatch) => {
    return {
        logoutUser: () => {
            return dispatch(logoutUser());
        },
    };
}

const mapStateToProps = (state) => {
    return {
        user: { ...state.user }
    };
}


class ProfilePage extends Component {

    state = {
        selectedKey: "orders",
        orders: [],
        payments: [],
        ordersLoading: false,
        paymentsLoading: false
    }

    orderSubscribe;

    componentDidMount() {
        this.loadPayments();
        this.loadOrders();
    }

    /**
     * Load Payments
     */
    loadPayments = async () => {
        const { user } = this.props
        if (!user) {
            return;
        }

        this.setState({
            ordersLoading: true
        });
        this.setState({
            paymentsLoading: true
        });
        let paymentTransactions = await PaymentController.getPayments(user.id);
        console.log(paymentTransactions)
        this.setState({
            paymentsLoading: false,
            payments: paymentTransactions
        });
    }

    /**
     * Build Payments List
     */
    buildPayments = () => {
        const { paymentsLoading, payments } = this.state;
        return (
            paymentsLoading ? <Spin tip="Loading Payments" size="large" /> : <List
                className="payment-body"
                dataSource={payments}
                renderItem={(payment) => <List.Item >
                    <List.Item.Meta title={`Payment ID - ${payment.id}`} description={payment.recordedAt} />
                    <Text strong>₹ {payment.totalAmount}</Text>
                </List.Item>}
            />
        );
    }

    /**
     * Load Orders
     */
    loadOrders = async () => {
        const { user } = this.props
        if (!user) {
            return;
        }

        this.setState({
            ordersLoading: true
        });
        this.orderSubscribe = OrderController.getOrders(user.id, (result) => {
            this.setState({
                ordersLoading: false,
                orders: []
            });
            this.setState({
                ordersLoading: false,
                orders: result
            });
        });

    }


    componentWillUnmount() {
        this.orderSubscribe();
    }
    /**
     * Build Orders List
     */
    buildOrders = () => {
        const { ordersLoading, orders } = this.state;
        const { user } = this.props;
        return (
            ordersLoading ? <Spin tip="Loading Orders" size="large" /> : <List
                className="order-body"
                dataSource={orders}
                renderItem={(order) => <OrderComponent user={user} order={order} />}
            />
        );
    }

    buildTabBar = () => {
        const { selectedKey } = this.state;
        return (
            <Menu
                className="profile-menu"
                selectedKeys={[selectedKey]}
                openKeys={[selectedKey]}
                mode="horizontal">
                <SubMenu
                    key="orders"
                    onTitleClick={() => this.setState({ selectedKey: "orders" })}
                    className={"active"}
                    title={
                        <span>
                            <Icon type="shopping-cart" />
                            <span>Orders</span>
                        </span>
                    } />
                <SubMenu
                    onTitleClick={() => this.setState({ selectedKey: "payments" })}
                    key="payments"
                    title={
                        <span>
                            <Icon type="pay-circle" />
                            <span>Payments</span>
                        </span>
                    }
                />
            </Menu>
        );
    }

    logOut = () => {
        const { logoutUser } = this.props;
        if (logoutUser) {
            logoutUser()
        }
    }

    render() {
        const { selectedKey } = this.state;
        const { user } = this.props

        return (
            <div className="profile-page">
                {user ? <div className="profile-header">
                    <List.Item>
                        <List.Item.Meta
                            title={<span className="title">{user.name}</span>}
                            description={<span className="description">{user.mobileNumber}</span>}
                        />
                        <Button onClick={() => {
                            let check = Modal.confirm({
                                title: "Do you want to logout?", onOk: () => {
                                    this.logOut()

                                }
                            });
                        }} type="dashed">Log Out</Button>
                    </List.Item>
                </div> : <div />}
                <div className="profile-body">
                    {this.buildTabBar()}
                    {selectedKey == "payments" ? this.buildPayments() : this.buildOrders()}
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);