import React, { Component } from 'react';
import LoginPage from './LoginPage';
import ProfilePage from './ProfilePage';
import { loginUser } from '../../actions/app';
import { connect } from 'react-redux';


const mapStateToProps = (state) => {
    const { user } = state;
    return {
        user
    };
}



class ProfileCheckPage extends Component {
    render() {
        const { user } = this.props;
        console.log(user)
        return (
            <div>
                {user ? <ProfilePage /> : <LoginPage />}
            </div>
        );
    }
}

export default connect(mapStateToProps)(ProfileCheckPage);