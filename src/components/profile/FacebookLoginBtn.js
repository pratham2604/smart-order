import React, { Component } from 'react';
import withFirebaseAuth from 'react-with-firebase-auth';
import firebase from 'firebase'
import 'firebase/auth';

import { Button } from 'antd';
import firebaseApp from '../../Firebase';

const providers = {
    facbebookProvider: new firebase.auth.FacebookAuthProvider(),
};

class FacebookLoginBtn extends Component {
    render() {
        const {
            user,
            signOut,
            signInWithFacebook,
        } = this.props;

        return (
            <Button style={{}} onClick={() => signInWithFacebook()} className="social-btn"><img src={'/facebook.png'} />Login With Facebook</Button>
        );
    }
}

const firebaseAppAuth = firebaseApp.auth();


export default withFirebaseAuth({
    providers,
    firebaseAppAuth,
})(FacebookLoginBtn);