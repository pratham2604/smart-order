import React, { Component } from 'react';
import CoverPage from './cover/CoverPage';
import ExploreCuisinesPage from './exploreCuisines/ExploreCuisinesPage';
import ContactForm from './contactForm/ContactForm';
import RestaurantList from '../restaurants/RestaurantList';
import './style.css';

class Home extends Component {
  render() {
    return (
      <div className="home">
        <CoverPage />
        <br />
        <RestaurantList />
        <ExploreCuisinesPage />
        <ContactForm />
      </div>
    );
  }
}

export default Home;