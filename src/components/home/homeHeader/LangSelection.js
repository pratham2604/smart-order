import * as React from "react";
import { Select } from "antd";
import LanguageController from "../../../controllers/language_controller";
// import { useTranslation } from "react-i18next";
// import i18next from "i18next";

const { Option } = Select;

const LangSelection = () => {
    const changeLanguage = (language) => {
        // i18next.changeLanguage(language);
        window.location.reload();
    };
    const currentLang = LanguageController.currentLanguage;
    const languageOptions = LanguageController.getLanguages();
    return <Select defaultValue={currentLang} onChange={changeLanguage}>
        {
            Object.keys(languageOptions).map((key) => <Option value={key}>{languageOptions[key]}</Option>)
        }
    </Select>
}

export default LangSelection;