import React, { Component } from 'react';
import { PageHeader, Button, Icon, Drawer, Dropdown, Avatar } from 'antd';
import Search from 'antd/lib/input/Search';
import { withRouter, Link } from 'react-router-dom';
import './style.css';
import AppConstants from '../../../helpers/app_constants';
import { Trans } from 'react-i18next';

class HomeHeader extends Component {

    state = {
        drawerVisible: false
    }

    buildResponsiveMenu = () => {
        const { drawerVisible } = this.state;

        return (
            <Drawer visible={drawerVisible} onClose={() => this.setState({ drawerVisible: false })}>
                <div className="responsive-menu">
                    <Link to="/" className="active"><Trans>{AppConstants.FIND_FOOD}</Trans></Link>
                    <Link to={AppConstants.CUISINE_PATH} className="active"><Trans>{AppConstants.CUISINES}</Trans></Link>
                    <Link to={AppConstants.RESTAURANTS_PATH}><Trans>{AppConstants.RESTAURANTS}</Trans></Link>

                    <Button shape="round" type="primary" className="checkout-btn">
                        Confirm Order
                    </Button>
                </div>
            </Drawer>
        );
    }

    buildDesktopMenu = () => {
        return (
            <PageHeader
                title={<Link to="/" style={{ color: 'black' }}>SmartOrdr</Link>}
                avatar={{ src: '/logo.png' }}
                extra={[
                    <Button href={'/profile'} type="link">Account</Button>,
                    <Button type="primary" href='/checkout' size="large" shape="round"><span className="checkout-text">Check Out </span><Icon type="shopping-cart" /></Button>,
                ]}
            />
        );
    }

    render() {
        return (
            this.buildDesktopMenu()
        );
    }
}

export default withRouter(HomeHeader);