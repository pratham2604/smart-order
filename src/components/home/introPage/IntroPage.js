import React, { Component } from 'react';
import './style.css'
import { Button, Icon } from 'antd';

class IntroPage extends Component {
    render() {
        return (
            <div className="intro-page">
                <img
                    className="intro-img"
                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTVc--V4PyJ96k6WuH2bBPyVYL5Cz3mObxTx0QVIfncNTYW8Hf-" />
                <div className="intro-para">
                    <h6>Blog</h6>
                    <h1>Why people<br />love us?</h1>
                    <h4>We choose to love these people because they are the only ones with whom we share an intimate connection deep enough that it can awaken and illuminate the darkest corners of ourselves</h4>
                    <Button type="link" className="more-btn">Read More <Icon type="right" /> </Button>
                </div>
            </div>
        );
    }
}

export default IntroPage;