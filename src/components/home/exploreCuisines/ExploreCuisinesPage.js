import React, { Component } from 'react';
import { Button, Icon } from 'antd';
import './style.css';
import RestaurantController from '../../../controllers/restaurant_controller';
import AppConstants from '../../../helpers/app_constants';
import LoadingShimmer from '../../common/LoadingShimmer';
import { Link } from 'react-router-dom';
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css'

class ExploreCuisinesPage extends Component {

    state = {
        isLoading: true,
        cuisines: []
    }

    async componentDidMount() {
        // Load Cuisines
        this.setState({
            isLoading: true
        })
        // Cuisines
        let cuisines = await RestaurantController.getCuisines()
        console.log(cuisines)
        this.setState({
            isLoading: false,
            cuisines
        })
    }

    render() {
        const { cuisines, isLoading } = this.state;
        cuisines.length = Math.min(cuisines.length, 4);
        const params = {
            slidesPerView: 'auto',
            spaceBetween: 30,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
        }
        return (
            <div className="explore-categories">
                <div className="explore-text-container">
                    <h2 className="title">
                        Exciting cuisines
                    </h2>
                    {/* <Link to={AppConstants.CUISINE_PATH}><Button type="link" className="explore-btn">View More <Icon type="right" /></Button></Link> */}
                </div>
                {isLoading ? <LoadingShimmer /> :
                    <div className="explore-category-list">
                        <Swiper {...params}>
                            {cuisines.map((cuisine) =>
                                <div style={{maxWidth: '10rem', display: 'inline-block'}}>
                                    <a href={AppConstants.getRestaurantByCuisineLink(cuisine.id)} className="explore-category">
                                        <div className="category">
                                            <img src={cuisine.image} />
                                            <h4>{cuisine.name}</h4>
                                        </div>
                                    </a>
                                </div>
                            )}
                        </Swiper>
                    </div>
                }
            </div>
        );
    }
}

export default ExploreCuisinesPage;