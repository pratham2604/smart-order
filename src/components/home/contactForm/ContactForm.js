import React, { Component } from 'react';
import './style.css';
import MapComponent from './MapComponent';
import { Container, Row, Col } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

class ContactForm extends Component {
    render() {
        return (
            <Container className="contact-form">
                <Row>
                    <Col sm="4">
                        <div className="logo-container">
                            <img src="/footer-logo.svg" className="logo"></img>
                        </div>
                    </Col>
                    <Col sm="8">
                        <Row>
                            <Col sm="4" className="data-container">
                                <h3 className="title">Download</h3>
                                <span className="description">
                                    <img src="/icon-apple.png" className="tech-icon"/>
                                    <span className="tech-text">iPhone</span> (Coming Soon)
                                </span>
                                <span className="description">
                                    <img src="/icon-android.png" className="tech-icon"/>
                                    <span className="tech-text">Android</span> (Coming Soon)
                                </span>
                            </Col>
                            <Col sm="4" className="data-container">
                                <h3 className="title">About Us</h3>
                                <h6 className="description">Careers</h6>
                            </Col>
                            <Col sm="4" className="data-container">
                                <h3 className="title">Get In Touch</h3>
                                <h6 className="description">
                                    <a href="mailto:contact@smartordr.com">contact@smartordr.com</a>
                                </h6>
                                <h6 className="title">Phone</h6>
                                <h6 className="description">+918007333666</h6>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col sm="3" className="footer-extended-row">
                        <h6 className="title">© 2020 SmartOrdr</h6>
                    </Col>
                    <Col sm="6" className="footer-extended-row">
                        <h6 className="title">
                            Terms of Service. Privacy Policy.
                        </h6>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default ContactForm;