import React, { Component } from 'react';
import { Map, GoogleApiWrapper } from 'google-maps-react';

class MaoComponent extends Component {
    render() {
        const { width, height } = this.props;
        const mapStyles = {
            width: width,
            height: height,
        };
        return (
            <Map
                google={this.props.google}
                zoom={8}
                style={mapStyles}
                initialCenter={{ lat: 47.444, lng: -122.176 }}
            />
        );
    }
}

export default GoogleApiWrapper({
    apiKey: "AIzaSyBGCv-NK-ORpVDpJSHWQgokxq0mmJToQe0"
})(MaoComponent);