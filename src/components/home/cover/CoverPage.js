import React, { Component } from 'react';
import { Drawer, message, Modal, Spin, notification, Icon } from 'antd';
import LocationSearch from '../../location/LocationSearch';
import LocationInput from '../../location/LocationInput';
import AppConstants from '../../../helpers/app_constants';
import Search from 'antd/lib/input/Search';
import TableController from '../../../controllers/table_controller';
import LocationController from '../../../controllers/location_controller';
import { bookTable, changeLocation } from '../../../actions/app';
import { connect } from 'react-redux';
import { Container, Row, Col, Button } from 'reactstrap';
import './style.css';

class CoverPage extends Component {
  state = {
    locationDrawerVisible: false,
    query: null,
    loadingRestaurant: null,
    isConfirming: false
  }

  openLocationDrawer = () => {
    this.setState({
      locationDrawerVisible: true
    });
  }

  closeLocationDrawer = () => {
    this.setState({
      locationDrawerVisible: false
    });
  }

  onSearch = () => {
    const { query } = this.state;
    window.location.href = AppConstants.getSearchLink(query);
  }

  getTableDetails = async (code) => {
    const { changeBookingType } = this.props;
    this.setState({
      loadingRestaurant: true
    })
    let table = await TableController.getTableDetails(code);
    this.setState({
      loadingRestaurant: false
    })
    if (table) {
      changeBookingType(table);
      window.location.href = AppConstants.getRestaurantWithTablePath(table.restaurantId, table.id);
    }
    else {
      message.error("Sorry table not found");
    }
  }

  onSearch = async (value) => {
    this.getTableDetails(value)
  }

  onLocationConfirmed = async (location) => {
    if (!location) {
        return
    }
    const { changeLocation } = this.props;
    this.setState({
        isConfirming: true
    });
    notification.open({ message: "Changing Location", icon: <Icon type="loading" />, key: "location" })
    let exactLocation = await LocationController.getLocationDetails(location.placeId);
    this.setState({
        isConfirming: false

    })
    notification.close("location")
    if (exactLocation) {
        changeLocation(exactLocation);
    }
    else {
        message.error("Please try again");
    }
  }

  render() {
    const { locationDrawerVisible, query, loadingRestaurant } = this.state;
    return (
      <div>
        <div className="cover-page">
          <h2 className="title">1 Click Smart Ordering Experience</h2>
          <Container className="selector-container">
            <Row>
              <Col lg={{size: 4, offset: 2}} >
                {/* <LocationInput /> */}
                <LocationSearch onLocationConfirmed={this.onLocationConfirmed} />
              </Col>
              <Col lg="4">
                <div className="cover-input">
                  <Search style={{ display: "block" }} onSearch={(e) => this.onSearch(e)} className="cover-search-input" enterButton="Submit" size="large" placeholder="Enter table code" />
                </div>
              </Col>
            </Row>
          </Container>
          
          {/* <div className="cover-input" style={{ marginTop: '1rem' }}>
            <Search style={{ display: "block" }} onSearch={(e) => this.onSearch(e)} className="cover-search-input" enterButton size="large" placeholder="Enter table code" />
          </div> */}
        </div>

        <Modal footer={[]} visible={loadingRestaurant} title="Loading Restaurant"><Spin /></Modal>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changeLocation: (location) => {
      let itemOrder = changeLocation(location);
      return dispatch(itemOrder);
    },
    changeBookingType: (table) => {
      return dispatch(bookTable(table));
    }
  }
}

export default connect(null, mapDispatchToProps)(CoverPage);