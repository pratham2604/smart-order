import React, { Component } from 'react';
import Shimmer from "react-shimmer-effect";
import './style.css';

class GridLoadingShimmer extends Component {

    buildGrid = () => {
        return (
            <div className="grid-shimmer">
                <Shimmer><div className="image" /></Shimmer>
                <div className="body">
                    <Shimmer><div className="title" /></Shimmer>
                    <Shimmer><div className="subtitle" /></Shimmer>

                </div>
            </div>);
    }

    render() {
        return (
            <div className="grid-shimmer-list">
                {this.buildGrid()}
                {this.buildGrid()}
                {this.buildGrid()}
                {this.buildGrid()}
                {this.buildGrid()}

            </div>
        );
    }
}

export default GridLoadingShimmer;