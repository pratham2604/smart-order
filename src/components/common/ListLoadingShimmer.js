import React, { Component } from 'react';
import { List, Skeleton } from 'antd';

class ListLoadingShimmer extends Component {
    render() {
        return (
            <List
                dataSource={[1, 2, 3, 4]}
                grid
                renderItem={(val) => <List.Item>
                    <Skeleton loading={true} active={false} paragraph={{ rows: 1 }} avatar={{ size: "large" }} />
                </List.Item>}
            />
        );
    }
}

export default ListLoadingShimmer;