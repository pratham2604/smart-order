import React, { Component } from 'react';
import Shimmer from "react-shimmer-effect";
import './style.css';

class LoadingShimmer extends Component {
    render() {
        return (
            <div className="loading-shimmer">
                <Shimmer>
                    <div className="image" />
                </Shimmer>
                <div className="body">
                    <Shimmer>
                        <div className="title" />

                    </Shimmer>
                    <Shimmer>
                        <div className="subtitle" />

                    </Shimmer>

                </div>
            </div>
        );
    }
}

export default LoadingShimmer;