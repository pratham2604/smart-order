import React, { Component } from 'react';
import ExploreCarousel from './ExploreCarousel';
import RestaurantList from '../restaurants/RestaurantList';
import RestaurantController from '../../controllers/restaurant_controller';
import { Spin, Row, Col } from 'antd';
import './style.css';

class ExplorePage extends Component {

    state = {
        loading: true,
        carouselImages: []
    }

    /**
     * Loading list of images for carousel
     * Loading list of restaurants
     */
    async componentDidMount() {
        this.setState({
            loading: true,
        });
        this.carouselImages = ["https://static.eazydiner.com/resized/1080X400/pages%2F845%2Fimage20190302135012.jpg", "https://d1m6qo1ndegqmm.cloudfront.net/uploadimages/coupons/7768-UtsavRestaurant_640x320_Banner.jpg", "http://blog.olacabs.com/wp-content/uploads/2018/09/Ola-Ahmedabad-Aliiance_Mailer.jpg"];
        this.setState({
            loading: false,
            carouselImages: this.carouselImages
        })
    }

    render() {
        const { restaurants, loading, carouselImages } = this.state;
        return (
            <div className="content">
                <ExploreCarousel images={carouselImages} />
                {loading ? <Spin /> : <Col>
                    <Row >
                        <RestaurantList />
                    </Row>
                </Col >}
            </div>
        );
    }
}

export default ExplorePage;