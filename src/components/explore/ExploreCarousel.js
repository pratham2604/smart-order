import React, { Component } from 'react';
import { Carousel } from 'antd';
import './style.css';

class ExploreCarousel extends Component {
    render() {
        let images = this.props.images || [];
        console.log("Images", images);
        return (
            <div className="explore-carousel">
                {images.map((image, index) => <div key={index}><img src={image} /></div>)}
            </div>
        );
    }
}

export default ExploreCarousel;