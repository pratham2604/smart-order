import React, { Component } from 'react';
import { Typography, List, Button, Icon, Empty } from 'antd';
import { connect } from 'react-redux';
import AddButton from './AddButton';
import { Link } from 'react-router-dom';
import { bookTable } from '../../actions/app';
import Order from '../../models/order';

const { Text, Title } = Typography;

const mapStateToProps = (state) => {
  const { order } = state;
  return {
    itemOrders: order.itemOrders
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    bookTable: (table) => {
      let result = bookTable(table)
      return dispatch(result);
    },
  }
}

class CartPage extends Component {
  state = {
    tableDataLoaded: false,
  }

  componentDidUpdate() {
    const { tableDataLoaded } = this.state;
    const { table, bookTable } = this.props;
    if (!tableDataLoaded && table) {
      bookTable(table);
      this.setState({
        tableDataLoaded: true,
      });
    }
  }

  render() {
    let { itemOrders, isMobile, currency } = this.props;
    console.log(currency)
    let itemMap = {};
    (itemOrders || []).forEach(element => {
      itemMap[element.id] = element;
    });
    let subAmount = Order.totalCost(new Order(null, null, itemOrders));

    if (isMobile) {
      if (itemOrders.length) {
        return (
          <div className="mobile-cart">
            <Link to="/checkout">
              <Button type="primary" block size="large" className="checkout-button">
                <div className="checkout-container">
                  <div className="checkout-details">
                    <div className="total">Total: ₹ {subAmount}</div>
                    <div className="note">Extra charges may apply</div>
                  </div>
                  <div className="checkout-arrow">
                    <div>Checkout<Icon type="arrow-right" /></div>
                  </div>
                </div>
              </Button>
            </Link>
          </div>
        )
      }
      return <div />
    }

    return (
      <div className="cart-page">
        <h1 className="header">Cart</h1>
        {itemOrders.length ?
          <div className="cart-body">
            <List
              dataSource={Object.values(itemMap).sort((a, b) => a.id < b.id)}
              renderItem={itemOrder =>
                <List.Item key={itemOrder.id}>
                  <div className="cart-item">
                    <Text strong className="item-name">{itemOrder.name}</Text>
                    <AddButton item={itemOrder} />
                  </div>
                </List.Item>}
            />
            <List.Item>
              <List.Item.Meta
                title={<Text style={{ fontSize: "0.9rem" }}>{"Sub Total"}</Text>}
                description={<Text style={{ fontSize: "0.7rem" }}>{"Taxes applicable as extra charges"}</Text>} />
              <Text style={{ fontSize: "1.1rem" }} strong>{`${CURRENCY_MAP[currency]} ${subAmount}`}</Text>
            </List.Item>
            <Link to="/checkout">
              <Button type="primary" block size="large">Checkout<Icon type="arrow-right" /></Button>
            </Link>
          </div> : <Empty />
        }
      </div>
    );
  }
}

const CURRENCY_MAP = {
  INR: '₹',
  DOLLAR: '$',
  EURO: '€',
  POUND: '£',
}

export default connect(mapStateToProps, mapDispatchToProps)(CartPage);