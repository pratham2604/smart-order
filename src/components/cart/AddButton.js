import React, { Component } from 'react';
import { Button, Icon, Typography } from 'antd';
import './style.css';
import { connect } from "react-redux";
import { addItem, removeItem, clearOrder } from '../../actions/app';
import RestaurantCustomizationDialog from '../restaurantDetail/RestaurantCustomizationDialog';

const { Text } = Typography;


const mapStateToProps = (state) => {
    const { order } = state;
    return {
        itemOrders: order.itemOrders,
        order: order
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        addItemOrder: (item) => {
            let itemOrder = addItem(item);
            return dispatch(itemOrder);
        },
        clearOrder: () => {
            return dispatch(clearOrder());
        },
        removeItemOrder: (item) => {
            let action = removeItem(item);
            return dispatch(action);
        },
    };
}

class AddButton extends Component {


    state = {
        showCustomization: false
    }

    increaseItemOrder = (item) => {
        const { addItemOrder, clearOrder } = this.props;

        const { order } = this.props;
        console.log(order)
        if (order && order.itemOrders && order.itemOrders.length > 0 && item.restaurantId != order.restaurantId) {
            let check = window.confirm("You are ordering from different restauant. This will clear your previous order in cart");
            if (check) {
                clearOrder();
            }
            else {
                return;
            }
        }

        if (this.hasCustomization(item)) {
            this.setState({
                showCustomization: true
            })
        }
        else
            addItemOrder(item);
    }

    render() {
        const { item, itemOrders, addItemOrder, removeItemOrder, currency } = this.props;
        const { showCustomization } = this.state;
        let count = itemOrders.filter((val) => val.id == item.id).length;
        return (
            <div>
                {
                    count ? (<div className="add-button">
                        <Icon className="add-action" type="minus" onClick={() => removeItemOrder(item)} />
                        <span className="add-count">{count}</span>
                        <Icon className="add-action" type="plus" onClick={() => this.increaseItemOrder(item)} />

                    </div>) : (<div className="add-button" onClick={() => this.increaseItemOrder(item)}>
                        <span className="add-label">
                            Add  <sup>
                                +
                </sup>
                        </span>
                    </div>)
                }
                <RestaurantCustomizationDialog visible={showCustomization} onConfirmed={(customizations) => {
                    if (customizations) {
                        let newItem = { ...item };
                        newItem.customizationsApplied = customizations;
                        addItemOrder(newItem)
                    }
                    this.setState({ showCustomization: false })
                }} restaurantItem={item} currency={currency}/>
                {this.hasCustomization(item) ? <Text className="customize-text">*Customizable</Text> : <div />}
            </div>
        );
    }

    hasCustomization(item) {
        return (Array.isArray(item.customizations) && item.customizations.length > 0) || item.allowChefNote;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddButton);