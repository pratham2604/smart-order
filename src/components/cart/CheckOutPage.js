import React, { Component } from 'react';
import { Typography, List, Button, Icon, Empty, Radio, Modal, message, Input } from 'antd';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import RestaurantItemComponent from '../restaurantDetail/RestaurantItemComponent';
import OrderController from '../../controllers/order_controller';
import PaymentController from '../../controllers/payment_controller';
import { changeBookingType, changePaymentMethod, clearOrder, bookTable, loginUser } from '../../actions/app';
import AppConstants from '../../helpers/app_constants';
import PaymentMethod from '../../models/payment_method';
import StripeCheckout from 'react-stripe-checkout';
import Order from '../../models/order';
import Search from 'antd/lib/input/Search';
import RestaurantController from '../../controllers/restaurant_controller';
import TableController from '../../controllers/table_controller';
import UserController from '../../controllers/user_controller';

const { Text, Title } = Typography;


const mapStateToProps = (state) => {
    return state;
}


const mapDispatchToProps = (dispatch) => {
    return {
        bookTable: (table) => {
            let result = bookTable(table)
            return dispatch(result);
        },
        changeBookingType: (bookingType) => {
            let result = changeBookingType(bookingType)
            return dispatch(result);
        },
        changePaymentMethod: (paymentMethod) => {
            let result = changePaymentMethod(paymentMethod)
            return dispatch(result);
        },
        clearOrder: () => {
            let result = clearOrder()
            return dispatch(result);
        },
        loginUser: (user) => {
            return dispatch(loginUser(user));
        }

    };
}


class CheckOutPage extends Component {

    state = {
        confirmingOrder: false,
        tableSearching: false,
    }

    async componentDidMount() {
        let order = this.props.order;
        const { restaurantId } = order;
        let restaurant = await RestaurantController.getRestaurantDetail(restaurantId);
        this.setState({
            restaurant,
        });
    }

    onToken = async (token, addresses) => {
        let id = token.id;
        let order = this.props.order;
        this.confirmOrder(id)
    };

    buildItems = () => {
        const { restaurant = {} } = this.state;
        const { currency = 'INR' } = restaurant;
        let order = this.props.order;
        let { itemOrders } = order;

        let itemMap = {};
        (itemOrders || []).forEach(element => {
            itemMap[element.id] = element;
        });
        return (
            <div className="item-list">
                <h1 level={1}>Shopping<br />Cart</h1>

                <div className="item-detail-list">
                    {<List dataSource={Object.values(itemMap).sort((a, b) => a.id < b.id)} renderItem={(itemOrder) => {
                        return (
                            <RestaurantItemComponent restaurantItem={itemOrder} currency={currency}/>
                        );
                    }} />}
                </div>

            </div>
        );
    }

    // Table Inout Number
    buildCodeInput = () => {
        const { order, bookTable } = this.props;
        const { tableSearching } = this.state;
        return <Search title={"Verify"} placeholder={"Enter code"} loading={tableSearching} onSearch={async (val) => {
            this.setState({
                tableSearching: true
            })
            let table = await TableController.getTableDetails(val);
            this.setState({
                tableSearching: false
            })
            if (!table) {
                window.alert("Please enter valid code")
                return;
            }
            if (table.restaurantId != order.restaurantId) {
                window.alert("Restaurant id and Order id does not match")
                return;
            }

            bookTable(table)

        }} enterButton />
    }

    buildDeliveryOptions = () => {
        let { order } = this.props;
        let bookingType = order.bookingType;
        const { changeBookingType } = this.props;
        console.log(bookingType)
        return (
            <div className="bill-body">
                <h5 className="subhead" level={3}>Booking Type</h5>
                <div>
                    <List.Item>
                        <List.Item.Meta description={!order.tableId ? this.buildCodeInput() : <span className="dark-light">Table ID - {order.tableId}</span>} title={<span className="dark">{"Dine in"}</span>} />
                        <Radio className="radio" checked={bookingType == AppConstants.DINE_IN} onClick={(e) => {
                            if (order.tableId)
                                changeBookingType(AppConstants.DINE_IN);
                            else {
                                window.alert("Please enter your table code");
                            }
                        }} />
                    </List.Item>
                    <List.Item>
                        <List.Item.Meta title={<span className="dark">{"Pickup"}</span>}
                        />
                        <Radio className="radio" checked={bookingType == AppConstants.ORDER_PICKUP} onClick={(e) => {
                            changeBookingType(AppConstants.ORDER_PICKUP);
                        }} />
                    </List.Item>
                </div>
            </div>
        );
    }

    buildBill = () => {
        let order = this.props.order;
        let { itemOrders } = order;
        const { restaurant = {} } = this.state;
        const { currency = 'INR' } = restaurant;
        const CURRENCY_MAP = {
            INR: '₹',
            DOLLAR: '$',
            EURO: '€',
            POUND: '£',
        }
        const currencyUnit =  CURRENCY_MAP[currency];       

        let itemMap = {};
        (itemOrders || []).forEach(element => {
            itemMap[element.id] = element;
        });
        let subAmount = 0;

        itemOrders.forEach((val) => {
            subAmount += parseInt(val.price || 0);
        })

        return (
            <div>
                <Title className="subhead" >Bill</Title>
                <List.Item>
                    <List.Item.Meta
                        title={<Text strong className="dark" style={{ fontSize: "1.1rem" }}>{"Sub Total"}</Text>}
                        description={<Text className="dark-light">{"Taxes applicable as extra charges"}</Text>} />
                    <div style={{ width: '6rem' }} />
                    <Text className="dark-light" style={{ fontSize: "1.4rem" }} strong>{`${currencyUnit} ${Order.totalCost(order)}`}</Text>
                </List.Item>
            </div>
        );
    }

    buildPaymentMethod = () => {
        let { order } = this.props;
        let currentPaymentMethod = order.paymentMethod;
        const { changePaymentMethod } = this.props;
        const paymentMethods = PaymentController.getPaymentMethods();
        return (
            <div className="bill-body">
                <span className="subhead">Payment Method</span>
                <div>
                    {
                        paymentMethods.map((paymentMethod) => <List.Item>
                            <List.Item.Meta
                                title={<span className="dark">{paymentMethod.title}</span>}
                                key={paymentMethod.id} />
                            <Radio checked={currentPaymentMethod && paymentMethod.id == currentPaymentMethod} onClick={(e) => {
                                changePaymentMethod(paymentMethod.id)
                            }} />
                        </List.Item>)
                    }
                </div>
            </div>
        );
    }

    confirmOrder = async (paymentId) => {
        const { order, clearOrder, user, loginUser } = this.props;
        const newOrder = { ...order };
        let newUser = user;
        if (!newUser) {
            if (!paymentId) {
                let canLogin = window.confirm("You are not logged in. Do you wish to continue?");
                newUser = await UserController.createtNewUser();

                if (canLogin) {
                    loginUser(newUser);
                }
                else {
                    return;
                }
            }
            else {
                newUser = await UserController.createtNewUser();
                loginUser(newUser);
            }
        }
        // return;
        newOrder.customerId = newUser.id;
        this.setState({
            confirmingOrder: true
        });
        await OrderController.confirmOrder(newUser, order, paymentId);
        clearOrder();
        this.props.history.replace('/');
        message.success("Order Placed");
        this.setState({
            confirmingOrder: false
        });
    }

    onChange = (e) => {
        const { order } = this.props;
        const { value } = e.target;
        order.chef_note = value;
    }

    buildChefNote = () => {
        return (
            <div className="bill-body" style={{marginBottom: '10px'}}>
                <div className="subhead" style={{marginBottom: '10px'}}>Note for chef</div>
                <Input onChange={this.onChange} />
            </div>
        )
    }

    // Confirmation Button
    buildConfirmButton = () => {
        const { order, user } = this.props;
        let newOrder = { ...order };
        let canConfirmOrder = OrderController.canConfirmOrder(newOrder);
        if (order.paymentMethod == PaymentMethod.ONLINE) {
            return <StripeCheckout
                amount={Order.totalCost(order) * 100}
                description={"Order by " + !user ? "Anonymous" : user.name}
                name="Smart Ordr"
                currency={"INR"}
                stripeKey="pk_test_PfshH7MDMVJrLRVqlPClzp3l008GUARsv6"
                token={this.onToken}
            />
        }
        return <Button size="large" onClick={() => this.confirmOrder()} disabled={!canConfirmOrder} type="primary" block >
            Confirm Order < Icon type="arrow-right" />
        </Button >
    }

    render() {
        let order = this.props.order;
        let { itemOrders } = order;
        let { confirmingOrder } = this.state
        return (
            <div className="checkout-page">
                {
                    itemOrders.length ? (
                        <div className="checkout-body">
                            {this.buildItems()}
                            <div className="dark-bill">
                                <h2>Delivery Options</h2>
                                {this.buildDeliveryOptions()}
                                <div className="divider"></div>
                                {this.buildPaymentMethod()}
                                <div className="divider"></div>
                                {this.buildChefNote()}
                                <div className="divider"></div>
                                {this.buildBill()}
                                {this.buildConfirmButton()}
                            </div>
                        </div>
                    ) : <Empty />
                }
                <Modal
                    title={'Confirming order'}
                    visible={confirmingOrder}
                    footer={[]}
                    destroyOnClose={true}
                    closable={false}
                    confirmLoading={confirmingOrder}
                />
            </div>
        );


    }

}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CheckOutPage));
