import React, { Component, Fragment } from 'react';
import { Modal, Checkbox, List, Radio, Button, Input } from 'antd';
import { Typography } from 'antd';

const { Title } = Typography;

class RestaurantCustomizationDialog extends Component {
  constructor(props) {
    super(props);
    const { restaurantItem = {} } = props;
    const { customizations = [] } = restaurantItem;
    const requiredCustomizations = customizations.filter(customization => !customization.showOptions && customization.requirement === 'required' && customization.isAvailable);
    this.state = {
      customizationsApplied: requiredCustomizations
    }
  }

  buildRadioList = (customization, currency) => {
    const { customizationsApplied } = this.state;
    const { name, requirement, options } = customization;
    return (
      <div>
        <span style={{fontWeight: 'bold', fontSize: '18px', color: '#000'}}>
          {name}
          {requirement === 'required' ? "*" : ''}
        </span>
        <div id={name}>
          {options.map(option => {
            const checked = customizationsApplied.filter(temp => temp.name == option.name).length === 1;
            return (
              <List.Item style={{paddingBottom: 0, display: 'flex'}}>
                <div style={{flex: '1', display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
                  <Radio value={option.name} onClick={this.onRadioSelect.bind(this, options, option)} checked={checked}>
                    {option.name}
                  </Radio>
                </div>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>{`${currency} ${option.price}`}</div>
              </List.Item>
            )
          })}
        </div>
      </div>
    );
  }

  onRadioSelect = (options, option) => {
    const { customizationsApplied } = this.state;
    const filteredCustomizations = customizationsApplied.filter(customization => !options.some(option => option.name === customization.name));
    filteredCustomizations.push(option);
    this.setState({
      customizationsApplied: filteredCustomizations,
    });
  }

  onChange = (e, val) => {
    const { customizationsApplied } = this.state;
    let newCustomizations = [...customizationsApplied];
    if (!e.target.checked) {
      newCustomizations = newCustomizations.filter((temp) => val.name != temp.name);
    } else {
      newCustomizations.push(val);
    }
    this.setState({
      customizationsApplied: newCustomizations,
    });
  }

  buildcheckList = (customization, currency) => {
    return (
      <div>
        <span style={{fontWeight: 'bold', fontSize: '18px', color: '#000'}}>
          {customization.name}
          {customization.requirement === 'required' ? "*" : ''}
        </span>
        <div id={customization.name}>
          {customization.options.map((val) =>
            <List.Item style={{paddingBottom: 0, display: 'flex'}}>
              <div style={{flex: '1', display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
                <Checkbox onChange={(e) => this.onChange(e, val)}>
                  {val.name}
                </Checkbox>
              </div>
              <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>{`${currency} ${val.price}`}</div>
            </List.Item>
          )}
        </div>
      </div>
    );
  }

  buildOptionalSingle = (customization, currency) => {
    return (
      <div style={{display: 'flex', margin: '1rem 0'}}>
        <div style={{flex: '1'}}>
          <Checkbox onChange={(e) => this.onChange(e, customization)}>
            <span style={{fontWeight: 'bold', fontSize: '18px', color: '#000'}}>{customization.name}</span>
          </Checkbox>
        </div>
        <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'flex-end'}}>{`${currency} ${customization.price}`}</div>
      </div>
    )
  }

  buildRequiredSingle = (customization, currency) => {
    return (
      <div style={{display: 'flex', margin: '1rem 0'}}>
        <div style={{flex: '1', display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
          <span style={{fontWeight: 'bold', fontSize: '18px', color: '#000'}}>{customization.name}</span>
        </div>
        <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>{`${currency} ${customization.price}`}</div>
      </div>
    )
  }

  canConfirm = () => {
    const { customizationsApplied } = this.state;
    const { restaurantItem } = this.props;
    const availableCustomizations = restaurantItem.availableCustomizations;
    let checkCanConfirm = true;
    (availableCustomizations || []).forEach((mainVal) => {
        if (mainVal.isRequired)
            if (customizationsApplied.length == 0) {
                checkCanConfirm = false
            }
            else
                customizationsApplied.forEach((val) => {
                    checkCanConfirm = checkCanConfirm && mainVal.customizations.filter((tempCustomization) => tempCustomization.name == val.name).length != 0;
                })
    });
    return checkCanConfirm;
  }

  onChange = (e) => {
    const { restaurantItem } = this.props;
    const { value } = e.target;
    restaurantItem.chef_note = value;
  }

  render() {
    const { restaurantItem, visible, onConfirmed, currency } = this.props;
    const { customizations, allowChefNote = false } = restaurantItem;

    if (!allowChefNote && (!customizations || customizations.length == 0)) {
      return <div/>
    }

    const { customizationsApplied } = this.state;
    const footer = <Button disabled={!this.canConfirm()} onClick={() => {
      onConfirmed(customizationsApplied)
    }} type="primary" icon="plus" block size="large">Add Item</Button>
    return (
      <Modal title={restaurantItem.name} visible={visible} onCancel={() => onConfirmed(null)} destroyOnClose={true} footer={footer}>
        {customizations
          .filter(customization => customization.isAvailable)
          .map(customization => {
            const { optionsSelectType, showOptions, requirement } = customization;
            const isRequired = requirement === 'required';
            const isMultiple = optionsSelectType === 'multiselect';
            if (showOptions && isMultiple) {
              return this.buildcheckList(customization, currency);
            } else if (showOptions && !isMultiple) {
              return this.buildRadioList(customization, currency);
            } else if (isRequired) {
              return this.buildRequiredSingle(customization, currency);
            } else {
              return this.buildOptionalSingle(customization, currency);
            }
          })
        }
        {allowChefNote &&
          <div>
            <div>
              <strong>Note For Chef</strong>
            </div>
            <Input onChange={this.onChange} />
          </div>
        }
      </Modal>
    );
  }
}

export default RestaurantCustomizationDialog;