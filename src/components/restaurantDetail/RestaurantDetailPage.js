import React, { Component, Fragment } from 'react';
import RestaurantController from '../../controllers/restaurant_controller';
import { Typography, Icon } from 'antd';
import RestaurantMenuPage from './RestaurantMenuPage';
import LoadingShimmer from '../common/LoadingShimmer';
import { Link } from 'react-router-dom';
import TableController from '../../controllers/table_controller';
import UserController from '../../controllers/user_controller';
import { connect } from 'react-redux';
import { bookTable, clearOrder, loginUser } from '../../actions/app';
const queryString = require('query-string');
const { Text } = Typography;

const mapDispatchToProps = (dispatch) => {
  return {
    bookTable: (table) => {
      let result = bookTable(table)
      return dispatch(result);
    },

    loginUser: (user) => {
      return dispatch(loginUser(user));
    },

    clearOrder: () => {
      let result = clearOrder()
      return dispatch(result);
    },
  };
}


const mapStateToProps = (state) => {
  return state;
}


class RestaurantDetailPage extends Component {
  state = {
    isLoading: true,
    restaurant: null,
    restaurantId: null
  }

  async componentDidMount() {
    console.log(window.location.search)
    // Getting Restaurant Id
    const parsed = queryString.parse(window.location.search);
    // if id not found them show error
    if (!parsed || !parsed['id']) {
      this.setState({
        error: "Restaurant not found"
      })
      return;
    }

    const { id, tableId, userId } = parsed;

    this.setState({
        isLoading: true,
        restaurantId: id,
        tableId,
        userId
    });
    await Promise.all([
      this.loadRestaurant(id),
    ]);

    if (parsed["tableId"]) {
      let tableId = parsed["tableId"];
      let table = await TableController.getTableDetails(tableId);

      if (!table) {
        window.alert("Please enter valid code")
        return;
      }
      if (table.restaurantId != id) {
        window.alert("Restaurant id and Table id does not match")
        return;
      }
      let { bookTable, order, clearOrder } = this.props
      if (order && order.restaurantId == id) {
        bookTable(table)
        // window.alert("Table ID added successfully");
      }
      else {
        let canClear = window.confirm("It seems that you are ordering from different table. Would you like to clear your cart");
        if (canClear) {
          clearOrder();
          bookTable(table)

        }
      }
    }

    if (parsed["userId"]) {
      let userId = parsed["userId"];
      let user = await UserController.getUserDetails(userId);

      if (!user) {
        window.alert("User not found")
        return;
      }

      const { loginUser } = this.props;
      loginUser(user);
    }
  }

  loadRestaurant = async (restaurantId) => {
    const restaurant = await RestaurantController.getRestaurantDetail(restaurantId);
    this.setState({
      isLoading: false,
      restaurant,
    });
  }

  renderRestaurantDetailsForMobile = () => {
    const { isLoading, restaurant } = this.state;
    return (
      <div className="mobile-component">
        {isLoading ?
          <LoadingShimmer /> :
          <Fragment>
            <div className="mobile-restaurant-detail">
              <Link to="/">
                <Icon type="arrow-left" className="back-icon" />
              </Link>
              <img src={restaurant.thumb} className="cover" />
            </div>
            <div className="mobile-restaurant-info">
              <div className="heading">
                <Text className="title">
                  <h2>{restaurant.name}</h2>
                </Text>
                <span className="rating-container">
                  <span className="rating">
                    <Icon type="star" theme="filled" className="icon-start" /> {restaurant.rating}
                  </span>
                </span>
              </div>
              <div className="activities">
                <div className="activity">
                  <Icon type="share-alt" />
                  <div>Share</div>
                </div>
                <div className="activity">
                  <Icon type="star" />
                  <div>Reviews</div>
                </div>
                <div className="activity">
                  <Icon type="camera" />
                  <div>Photos</div>
                </div>
                <div className="activity">
                  <Icon type="heart" />
                  <div>Visited</div>
                </div>
              </div>
              <div className="address-contianer">
                <Text className="address">{restaurant.location.address}</Text>
                <div className="sub-detail">
                  <span>{restaurant.isCashAllowed ? "Cash" : "Online"}</span>
                  <span className="label"> Payment Method</span>
                </div>
              </div>
            </div>
          </Fragment>
        }
      </div>
    )
  }

  buildRestaurantDetails = () => {
    const { isLoading, restaurant } = this.state;

    return (
      <Fragment>
        <div className="dektop-component restaurant-detail-container">
          {isLoading ?
            <LoadingShimmer /> :
            <div className="restaurant-detail">
              <img src={restaurant.thumb} className="cover" />
              <div className="body">
                <Text className="title">{restaurant.name}</Text>
                <Text ellipsis className="description">{restaurant.location.address}</Text>
                <br />
                <Text ellipsis className="description">{(restaurant.cuisines || []).map((cuisine) => cuisine.name).join(", ")}</Text>
                <div className="details">
                  <div className="sub-detail">
                    <span className="name"><Icon type="star" theme="filled" /> {restaurant.rating}</span>
                    <span className="label">Ratings</span>
                  </div>
                  <div className="sub-detail">
                    <span>{restaurant.isCashAllowed ? "Cash" : "Online"}</span>
                    <span className="label">Payment Method</span>
                  </div>
                  <div className="sub-detail">
                    <span>₹ {restaurant.averageCostForTwo}</span>
                    <span className="label">Cost for two</span>
                  </div>
                </div>
              </div>
            </div>
          }
        </div>
        {this.renderRestaurantDetailsForMobile()}
      </Fragment>
    );
  }

  render() {
    const { restaurantId, tableId, restaurant } = this.state;
    return (
      <div>
        {this.buildRestaurantDetails()}
        <div>
          {restaurantId && <RestaurantMenuPage restaurantId={restaurantId} tableId={tableId} restaurant={restaurant}/>}
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantDetailPage);