import React, { Component } from 'react';
import { Card, List, Button, Avatar, Typography } from 'antd';
import RestaurantCustomizationDialog from './RestaurantCustomizationDialog';
import AddButton from '../cart/AddButton';

const { Text } = Typography;

class RestaurantItemComponent extends Component {

    state = {
        isCustomizationShowing: false,
    }

    // Open Customization Model
    showCustomization = () => {
        this.setState({
            isCustomizationShowing: true
        })
    }

    // Close Customization Model
    closeCustomization = () => {
        this.setState({
            isCustomizationShowing: false
        })
    }

    render() {
        const { restaurantItem, currency } = this.props;
        const { isCustomizationShowing } = this.state;
        console.log(currency)

        if (!restaurantItem) {
            return (
                <div></div>
            );
        }
        return (
            <div style={{ width: "100%" }}>
                <div className="restaurant-item">
                    {restaurantItem.thumbnail ?
                        <img src={restaurantItem.thumbnail} className="thumbnail" /> :
                        <div className="thumbnail"></div>
                    }
                    <div className="body">
                        <div>
                            <span className="title">
                                {restaurantItem.name}
                            </span>
                            <span className="description">
                                {restaurantItem.ingredient}
                            </span>
                        </div>
                        <span className="price">
                            {CURRENCY_MAP[currency]} {restaurantItem.price}
                        </span>
                    </div>
                    <AddButton item={restaurantItem} currency={CURRENCY_MAP[currency]}/>
                </div>
                <RestaurantCustomizationDialog
                    currency={CURRENCY_MAP[currency]}
                    isShowing={isCustomizationShowing}
                    onClose={this.closeCustomization}
                    restaurantItem={restaurantItem} />
            </div>
        );
    }
}

const CURRENCY_MAP = {
    INR: '₹',
    DOLLAR: '$',
    EURO: '€',
    POUND: '£',
}

export default RestaurantItemComponent;