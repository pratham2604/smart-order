import React, { Component, Fragment } from 'react';
import { List, Empty } from 'antd';
import RestaurantController from '../../controllers/restaurant_controller';
import TableController from '../../controllers/table_controller';

import RestaurantItemComponent from './RestaurantItemComponent';
import CartPage from '../cart/CartPage';
import './style.css'
import LoadingShimmer from '../common/LoadingShimmer';

class RestaurantMenuPage extends Component {
  state = {
    isLoading: true,
    restaurantMenu: null
  }


  componentDidMount() {
    let { restaurantId, tableId } = this.props;
    this.loadRestaurantMenu(restaurantId);
    this.loadTableDetails(tableId);
  }

  loadTableDetails = async (tableId) => {
    this.setState({
      isLoading: true,
    });
    let table = await TableController.getTableDetails(tableId);
    this.setState({
      isLoading: false,
      table,
    })
  }

  loadRestaurantMenu = async (restaurantId) => {
    this.setState({
      isLoading: true
    })
    let restaurantMenu = await RestaurantController.getRestaurantMenu(restaurantId);
    this.setState({
      isLoading: false,
      restaurantMenu: restaurantMenu
    })
  }

  render() {
    const { restaurantMenu, isLoading, table } = this.state;
    const { restaurant } = this.props;
    const { currency = 'INR' } = restaurant || {};

    if (isLoading) {
      return <div style={{ "padding": "2rem" }} >
          <LoadingShimmer />
      </div>
    }
    if (!restaurantMenu) {
      return <Empty />
    }
    const items = restaurantMenu.items || [];
    return (
      <Fragment>
        <div className="dektop-component restaurant-menu-page">
          <div className="restaurant-menu">
            <h1 className="header">
              Restaurant Menu
            </h1>
            <div className="restaurant-menu-items">
              <List dataSource={items} renderItem={(item) => <List.Item style={{ width: "100%" }}><RestaurantItemComponent currency={currency} restaurantItem={item} /></List.Item>}>
              </List>
            </div>
          </div>
          <CartPage table={table} currency={currency}/>
        </div>
        <div className="mobile-component">
          <h2 className="restaurant-menu-header">
            Restaurant Menu
          </h2>
          <div className="restaurant-mobile-menu-items">
            <List dataSource={items} renderItem={item => 
              <List.Item style={{ width: "100%", paddingTop: 0 }}>
                <RestaurantItemComponent restaurantItem={item} />
              </List.Item>}>
            </List>
          </div>
          <div className="mobile-padding-fix"></div>
          <div className="checkout-container">
            <CartPage isMobile table={table} currency={currency}/>
          </div>
        </div>
      </Fragment>
    );
  }
}


export default RestaurantMenuPage;