import React, { Component } from 'react';
import StripeCheckout from 'react-stripe-checkout';

class PaymentPage extends Component {
    onToken = (token, addresses) => {
        console.log(token);
    };

    render() {
        return (
            <StripeCheckout
                amount="500"
                description="Order Id - 123456"
                name="Smart Ordr"
                stripeKey="pk_test_PfshH7MDMVJrLRVqlPClzp3l008GUARsv6"
                token={this.onToken}
            />
        )
    }
}

export default PaymentPage;