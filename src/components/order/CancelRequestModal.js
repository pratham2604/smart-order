import React, { Component } from 'react';
import { Modal, message, Form, Input } from 'antd';
import OrderController from '../../controllers/order_controller';

class CancelRequestModal extends Component {

    state = {
        reason: null,
        isConfirming: false,
    }

    makeCancelRequest = async () => {
        const { orderId, userId, restaurantId, onConfirmed } = this.props;
        const { reason } = this.state;
        if (!reason) {
            message.error("Please enter a reason");
            return;
        }
        this.setState({
            isConfirming: true
        })
        await OrderController.makeCancelRequest(orderId, userId, restaurantId, reason);
        this.setState({
            isConfirming: false
        })
        message.success("Cancel request sent");
        onConfirmed();
    }

    render() {
        const { visible, onConfirmed } = this.props;
        const { isConfirming } = this.state;
        return (
            <Modal title={"Request Cancellation"} visible={visible} confirmLoading={isConfirming} destroyOnClose={true} onOk={() => this.makeCancelRequest()} onCancel={() => onConfirmed()} >
                <Form>
                    <Form.Item label={"Reason"}>
                        <Input size="large" onChange={(e) => this.setState({ "reason": e.target.value })}
                            name={"Reason"}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        );
    }
}

export default CancelRequestModal;