import React, { Component } from 'react';
import { List, Button, Modal, Card } from 'antd';
import Order from '../../models/order';
import OrderRatingDialog from './OrderRatingDialog';
import OrderDetailsModal from './OrderDetailsModal';
import CancelRequestModal from './CancelRequestModal';

class OrderComponent extends Component {

    state = {
        isRating: false,
        showOrderDetails: false,
        showCancel: false
    }

    rateOrder = async () => {
        this.setState({
            isRating: true,
        })


    }

    onDone = () => {
        this.setState({
            isRating: false,
        })
    }

    render() {
        const { order, user } = this.props;
        const { isRating, showOrderDetails, showCancel } = this.state;
        return (
            <div key={order.id}>
                <Card hoverable={true} >
                    <List.Item onClick={() => this.setState({ showOrderDetails: true })} >
                        <List.Item.Meta title={`${(Order.fromJson(order)).name}`} description={(new Date(order.recordedAt).toDateString())} />
                        <Button onClick={(e) => { this.rateOrder(); e.stopPropagation() }} type="link">Rate</Button>
                    </List.Item>
                </Card>
                <OrderRatingDialog user={user} order={order} visible={isRating} onDone={() => this.setState({ isRating: false })} />
                <OrderDetailsModal visible={showOrderDetails} onCancelRequest={() => this.setState({ showCancel: true })} order={order} onClose={() => this.setState({ showOrderDetails: false })} />
                <CancelRequestModal visible={showCancel} orderId={order.id} userId={user.id} restaurantId={order.restaurantId} onConfirmed={() => this.setState({ showCancel: false })} />
            </div>
        );
    }
}

export default OrderComponent;