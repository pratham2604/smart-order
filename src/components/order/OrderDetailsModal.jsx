import React, { Component } from 'react';
import { Modal, List, Button, Icon, Dropdown, Menu, message, Tag } from 'antd';
import OrderController from '../../controllers/order_controller';

const CONFIRMED = 0;
const DELIVERED = 1;
const NOT_CONFIRMED = -2;
const CANCELLED = -1;
const IN_KITCHEN = 2

class OrderDetailsModal extends Component {

    state = {
        loading: false,
        cancelling: false,
        statusCode: null,
        showCancel: false
    }

    componentDidMount() {
        const { order } = this.props;
        this.setState({ statusCode: order.statusCode })
    }

    handleMenuClick = (e) => {
        this.setState({
            statusCode: e.key
        })
    }

    orderStatus = (statusCode) => {
        console.log("Order Status", statusCode)

        switch (Number.parseInt(statusCode)) {
            case CONFIRMED:
                return "Confirmed";
            case CANCELLED:
                return "Cancelled";
            case DELIVERED:
                return "Delivered";
            case IN_KITCHEN:
                return "In Kitchen";
            default:
                return "Not Confirmed";
        }
    }

    orderStatusColor = (statusCode) => {

        switch (Number.parseInt(statusCode)) {
            case CONFIRMED:
                return "orange";
            case CANCELLED:
                return "red";
            case DELIVERED:
                return "green";
            default:
                return "magenta";
        }
    }

    onSaveChanges = async () => {
        const { order, onClose } = this.props;
        const { statusCode } = this.state;
        let newOrder = { ...order };
        newOrder.statusCode = statusCode;
        this.setState({
            loading: true
        })
        await OrderController.updateOrder(newOrder);
        this.setState({
            loading: false
        })
        onClose()
    }



    render() {
        const { visible, order, onClose, onCancelRequest } = this.props;
        const { statusCode, loading, cancelling } = this.state;
        console.log("Order Status", this.orderStatus(statusCode))
        return (
            <Modal onCancel={onClose} visible={visible} destroyOnClose={true} title={"Order Details"} footer={[
                <Button disabled={order.statusCode == CANCELLED || order.statusCode == DELIVERED} loading={cancelling} onClick={onCancelRequest} type="danger">
                    Cancel Request
                </Button>,
            ]}>
                <h2>
                    Items
                </h2>
                {order.itemOrders.map((val) => <List.Item key={val.id}><List.Item.Meta title={val.name} />{val.price}</List.Item>)}
                <h2>
                    Details
                </h2>
                <List.Item><List.Item.Meta title={"Placed At"} />{new Date(order.recordedAt).toDateString()}</List.Item>
                <List.Item><List.Item.Meta title={"Promo Code Applied"} />{"None"}</List.Item>
                <h2>
                    Status
                </h2>
                <List.Item><List.Item.Meta title={"Order Status"} />
                    <Tag style={{ fontSize: '0.8rem', padding: '0.5rem' }} color={this.orderStatusColor(statusCode)}>  {this.orderStatus(statusCode)}
                    </Tag>
                </List.Item>
                {order.cancelReason ? <List.Item><List.Item.Meta title={"Cancel Reason"} />
                    {order.cancelReason}
                </List.Item> : <div />}
            </Modal>
        );
    }
}

export default OrderDetailsModal;