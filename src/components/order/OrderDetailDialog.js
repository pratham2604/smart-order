import React, { Component } from 'react';
import { Modal, Button, List } from 'antd';

class OrderDetailDialog extends Component {
    render() {
        let { order } = this.props;
        return (
            <Modal title={"Order Details"} footer={[<Button type={"danger"}>Cancel Request</Button>]}>
                <h3>
                    Items
                </h3>
                <List dataSource={order.itemOrders} renderItem={(item) => <List.Item.Meta title={item.name} />} />
                <h3>
                    Delivery Type
                </h3>
                <List.Item>
                    <List.Item.Meta title={"Dine In"} />
                </List.Item>
                <h3>
                    Total Cost
                </h3>
                <List.Item>
                    <List.Item.Meta title={"Subtotal"} />
                </List.Item>
                <List.Item>
                    <List.Item.Meta title={"Subtotal"} />
                </List.Item>
                <List.Item>
                    <List.Item.Meta title={"Total Cost"} />
                </List.Item>
            </Modal>
        );
    }
}

export default OrderDetailDialog;