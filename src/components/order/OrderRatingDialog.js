import React, { Component } from 'react';
import { Modal, Form, Input, Icon, Spin, message } from 'antd';
import Rating from 'react-rating';
import OrderReviewController from '../../controllers/order_review_controller';
import OrderReview from '../../models/order_review';

class OrderRatingDialog extends Component {

    state = {
        isRating: false,
        rating: 0,
        description: null,
        isLoading: true,
        id: null
    }

    componentDidMount() {
        this.getOrderReview();
    }

    addOrderReview = async () => {
        const { order, user, onDone } = this.props;
        const { rating, description, id } = this.state;
        if (!rating) {
            message.error("Please enter rating");
            return;
        }
        this.setState({ isRating: true });
        let review = new OrderReview(id, order.restaurantId, order.id, user.id, rating, description, user, null);
        await OrderReviewController.addOrderReview(review);
        this.setState({ isRating: false });
        onDone();
    }

    getOrderReview = async () => {
        const { order, user, onDone } = this.props;

        this.setState({ isLoading: true });
        let orderReview = await OrderReviewController.getOrderReview(user.id, order.id);
        this.setState({ isLoading: false, description: orderReview["description"], id: orderReview.id, rating: orderReview["rating"] })
    }


    render() {
        const { onDone, order, visible } = this.props;

        const { isLoading, isRating, rating, description } = this.state;

        return (
            <Modal
                visible={visible}
                confirmLoading={isRating}
                onCancel={() => onDone()}
                destroyOnClose={true}
                title={"Rate your order"}
                okButtonProps={{ disabled: rating == 0 }}
                onOk={() => { this.addOrderReview() }}>
                {isLoading ? <Spin /> : <Form>
                    <Rating
                        initialRating={rating}
                        onClick={(value) => { this.setState({ rating: value }) }}
                        emptySymbol={<Icon type="star" theme={"outlined"} style={{ fontSize: '1.5rem' }}></Icon>}
                        fullSymbol={<Icon type="star" theme={"twoTone"} style={{ color: 'yellow', fontSize: '1.5rem' }}></Icon>} onChange={() => { }} start={0} stop={5} />
                    <br />
                    <Form.Item label="Description" help={"Optional"} >
                        <Input value={description} onChange={(e) => {
                            this.setState({
                                description: e.target.value
                            })
                        }} />
                    </Form.Item>
                </Form>}
            </Modal>
        );
    }
}

export default OrderRatingDialog;