import React, { Component } from 'react';
import { Trans } from 'react-i18next';
import AppConstants from '../../helpers/app_constants';
import './style.css';
import GridLoadingShimmer from '../common/GridLoadingShimmer';
import RestaurantComponent from '../restaurants/RestaurantComponent';
import { Empty, Button } from 'antd';
import RestaurantController from '../../controllers/restaurant_controller';
import { Link } from 'react-router-dom';
import Text from 'antd/lib/typography/Text';

class CuisinePage extends Component {

    state = {
        loading: true,
        popularRestaurants: [],
        cuisines: []
    }

    async componentDidMount() {
        this.setState({
            loading: true
        })

        await Promise.all([
            this.loadRestaurants(),
            this.loadCuisines(),
        ])

        this.setState({
            loading: false
        })
    }

    // Loading Restaurants
    async loadRestaurants() {
        let popularRestaurants = await RestaurantController.getRestaurants();
        this.setState({
            popularRestaurants: popularRestaurants,
        })
    }

    // Loading Cuisines
    async loadCuisines() {
        let cuisines = await RestaurantController.getCuisines();
        this.setState({
            cuisines: cuisines,
        })
    }

    buildCuisine = (cuisine) => {
        return (
            <Link to={AppConstants.getRestaurantByCuisineLink(cuisine.id)}>
                <div className="cuisine-card">
                    <img
                        alt={cuisine.name}
                        src={cuisine.image}
                    />
                    <div className='cuisine-card-body'>
                        <Text className="title" strong>{cuisine.name}</Text>
                    </div>
                </div>
            </Link>
        );
    }

    render() {
        const { popularRestaurants, loading, cuisines } = this.state;
        console.log(cuisines)
        return (
            loading ? <GridLoadingShimmer /> : <div className="cuisine-body">
                <div className="restaurant-list">
                    <h3>
                        <Trans>{AppConstants.POPULAR}</Trans>
                        <br />
                        <Trans>{AppConstants.RESTAURANTS}</Trans>
                    </h3>
                    <div style={{ height: "1rem" }} />
                    <div className="restaurant-name-list">
                        {popularRestaurants.map((restaurant) =>
                            <Button type="link" className="restaurant-name" href={AppConstants.getRestaurantPath(restaurant.id)}>{restaurant.name}</Button>)
                        }
                    </div>
                </div>
                <div className="cuisine-list">
                    {cuisines.length == 0 ? <Empty /> : cuisines.map((cuisine) => this.buildCuisine(cuisine))}
                </div>
            </div>
        );
    }
}

export default CuisinePage;