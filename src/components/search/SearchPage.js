import React, { Component } from 'react';
import RestaurantController from '../../controllers/restaurant_controller';
import './style.css';
import { AutoComplete, Input, Button, Icon, List, Avatar } from 'antd';
import { Link } from 'react-router-dom';
import ListLoadingShimmer from '../common/ListLoadingShimmer';
import { Typography } from 'antd';

const { Text } = Typography;


class SearchPage extends Component {

    state = {
        isSearching: false,
        query: "",
        restaurants: [],
        topRatedRestaurants: [],
        selectedRestaurant: null,
    }

    /**
     * Set isSearching to true
     */
    async componentDidMount() {

        // Load Top Restaurants
        this.loadTopRestaurants();
    }

    /**
    * Search Page
    */
    search = async (query) => {
        this.setState({
            isSearching: true,
            query: query
        })
        let result = await RestaurantController.searchRestaurants(query);
        this.setState({
            restaurants: result,
            isSearching: false
        })

    }

    /**
     * Load Top Restaurants
     */
    loadTopRestaurants = async () => {
        let result = await RestaurantController.searchRestaurants("");
        this.setState({
            topRatedRestaurants: result
        });
    }

    /**
     * Default Drop
     */
    buildDataSource = () => {
        const { topRatedRestaurants, query } = this.state;
        return (
            <div className="search-suggestions">
                <Text strong type="secondary">Top Results</Text>
                <List
                    dataSource={topRatedRestaurants}
                    renderItem={(restaurant) => (
                        <List.Item className="suggestion">
                            <Link to={RestaurantController.getRestaurantLink(restaurant.id)}>
                                <List.Item.Meta
                                    avatar={<img src={restaurant.thumb} />}
                                    title={restaurant.name}
                                    description={restaurant.description}
                                />
                            </Link>

                        </List.Item>)}
                />
            </div>
        );
    }

    /**
     * Build Restaurants
     */
    buildRestaurants = () => {
        const { restaurants } = this.state;
        return (
            <div>
                <Text strong type="secondary">{`Search Results (${restaurants.length})`}</Text>
                <List
                    dataSource={restaurants}
                    renderItem={(restaurant) => <List.Item className="search-item" actions={[<Button type="dashed" icon="arrow" >View </Button>]}>
                        <List.Item.Meta

                            title={restaurant.name}
                            description={restaurant.description}
                            avatar={<Avatar shape={"square"} size={"large"} className="avatar" src={restaurant.thumb} />}
                        />
                    </List.Item>}
                />
            </div>

        );
    }

    render() {
        const { isSearching, restaurants, query } = this.state;
        console.log(isSearching)
        return (
            <div className="search-page">
                <AutoComplete
                    className="search-input"
                    size="large"
                    dataSource={[]}
                    showArrow={true}
                    onChange={this.search}
                    onSelect={(val) => { this.setState({ query: val }) }}
                >
                    <Input
                        placeholder="Type name of your favourite restaurant"
                        suffix={
                            <Button
                                className="search-btn"
                                style={{ marginRight: -12 }}
                                size="large"

                                type="primary"
                            >
                                <Icon type={!isSearching ? "search" : "loading"} />
                                Search
                            </Button>
                        }
                    />
                </AutoComplete>
                {query ? (<div className="search-results">
                    {isSearching ? <ListLoadingShimmer /> : this.buildRestaurants()}

                </div>) : this.buildDataSource()}
            </div>
        );
    }
}

export default SearchPage;