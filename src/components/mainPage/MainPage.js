import React, { Component } from 'react';
import HomeHeader from '../home/homeHeader/HomeHeader';
import { Router, Route, Switch } from "react-router";
import Home from '../home/Home';
import RestaurantList from '../restaurants/RestaurantList';
import RestaurantController from '../../controllers/restaurant_controller';
import RestaurantDetailPage from '../restaurantDetail/RestaurantDetailPage';
import CheckOutPage from '../cart/CheckOutPage';
import ProfileCheckPage from '../../components/profile/ProfileCheckPage';
import AppConstants from '../../helpers/app_constants';
import CuisinePage from '../cuisines/CuisinePage';


class MainPage extends Component {
    render() {
        return (
            <Switch>

                <div className="main-page">
                    <HomeHeader />
                    <Route exact path={'/profile'} component={ProfileCheckPage} />
                    <Route exact path={AppConstants.CUISINE_PATH} component={CuisinePage} />

                    <Route exact path={AppConstants.CHECKOUT_PATH} component={CheckOutPage} />
                    <Route exact path={AppConstants.RESTAURANT_PATH} component={RestaurantDetailPage} />
                    <Route exact path={AppConstants.RESTAURANTS_PATH} component={RestaurantList} />
                    <Route exact path='/' component={Home} />
                </div>
            </Switch>

        );
    }
}

export default MainPage;