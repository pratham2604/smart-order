import React, { Component } from 'react';
import { Drawer, Button, Icon, message, Modal, notification } from 'antd';
import LocationSearch from './LocationSearch';
import { connect } from 'react-redux';
import { changeLocation } from '../../actions/app';
import LocationController from '../../controllers/location_controller';

// Getting current location from state
const mapStateToProps = (state) => {
    return { currentLocation: state.location };
}

// Dispatching new location
const mapDispatchToProps = (dispatch) => {
    return {
        changeLocation: (location) => {
            let itemOrder = changeLocation(location);
            return dispatch(itemOrder);
        },

    };
}



class LocationInput extends Component {


    state = {
        searchVisible: false,
        isConfirming: false
    }

    // Getting place details from location
    onLocationConfirmed = async (location) => {
        if (!location) {
            return
        }
        const { changeLocation } = this.props;
        this.setState({
            isConfirming: true
        });
        notification.open({ message: "Changing Location", icon: <Icon type="loading" />, key: "location" })
        let exactLocation = await LocationController.getLocationDetails(location.placeId);
        this.setState({
            isConfirming: false

        })
        notification.close("location")
        if (exactLocation) {
            changeLocation(exactLocation);
        }
        else {
            message.error("Please try again");
        }
    }

    render() {
        const { currentLocation, changeLocation } = this.props;
        const { searchVisible } = this.state;
        let child;
        // If current location is not selected 
        if (!currentLocation) {
            child = "Select Location";
        }
        else
            child = currentLocation.description;
        // On click of location text make search drawer visible
        return <div style={{ display: 'block' }}>
            <Button style={{ color: 'white', fontSize: '1rem', fontWeight: '500' }} type="link" onClick={() => this.setState({ searchVisible: true })} >
                <img src="/images/icons/location.png" style={{ width: "1.2rem", height: "1.2rem" }} />
                <div style={{ width: "1rem", display: "inline-block" }} />
                {child}</Button>
            <LocationSearch visible={searchVisible} onLocationConfirmed={
                (location) => {

                    this.setState({ searchVisible: false })
                    this.onLocationConfirmed(location)
                }} />
        </div>
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationInput);