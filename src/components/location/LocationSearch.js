import React, { Component } from 'react';
import { Input, List, Drawer } from 'antd';
import LocationController from '../../controllers/location_controller';
import { connect } from 'react-redux';
import { changeLocation } from '../../actions/app';
import AsyncSelect from 'react-select/async';
import './style.css';
const { Search } = Input;

const Label = (props) => {
  const { title, description } = props.data;
  return (
    <div>
      <div>{title}</div>
      {/*<div>{description}</div>*/}
    </div>
  )
}

const loadOptions = (inputValue, callback) => {
  LocationController.getSuggestions(inputValue).then((result) => {
    const options = result.map(suggestion => Object.assign({}, {label: <Label data={suggestion} />, value: suggestion.placeId, info: suggestion}))
    callback(options);
  }).catch((err) => {
    console.log(err);
  });
};

class LocationSearch extends Component {
  state = {
    query: "",
    loading: false,
    suggestions: [],
    selectedLocation: null,
  }

  onSearch = async (query) => {
    return query;
  }

  onChange = (option) => {
    const { onLocationConfirmed } = this.props;
    const { info } = option;
    onLocationConfirmed(info);
  }

  render() {

    return (
      <div className="search-body">
        <AsyncSelect
          placeholder="Search Location"
          cacheOptions
          defaultOptions
          onInputChange={this.onSearch}
          loadOptions={loadOptions}
          onChange={this.onChange}
        />
      </div> 
    );
  }
}

export default LocationSearch;