import { ADD_ITEM, INITIALIZE_ORDER, REMOVE_ITEM, CLEAR_ORDER, CHANGE_BOOKING_TYPE, CHANGE_PAYMENT_METHOD, LOGIN_USER, LOGOUT_USER, CHANGE_LOCATION, BOOK_TABLE } from '../../constants/cart';
import Order from '../../models/order';
import { CartAction } from '../../actions/app';
import AppConstants from '../../helpers/app_constants';
import User from '../../models/user';
import uuid from 'uuid';

const constOrder = new Order(null, null, [], null, null, null, null, null)

const initialState = {
    order: { ...constOrder },
}

///
/// Cart Reducers
///

/**
 * 
 * @param {CartAction} action 
 */
export function appReducer(state = initialState, action) {

    // Login user
    if (action.type == LOGIN_USER) {
        let user = { ...action.payload };
        let newState = { ...state };
        console.log(user)
        newState.user = user;
        return newState;
    }
    else if (action.type == LOGOUT_USER) {
        let newState = { ...state };
        newState.user = null;
        console.log(newState)
        return newState;
    }

    if (action.type == CHANGE_BOOKING_TYPE) {
        let bookingType = action.payload;
        let tempState = { ...state };
        let tempOrder = { ...tempState.order };
        tempOrder.bookingType = bookingType;
        let newState = { ...state };
        newState.order = tempOrder;

        return newState;
    }
    if (action.type == CHANGE_PAYMENT_METHOD) {
        let paymentMethod = action.payload;
        let tempState = { ...state };
        let tempOrder = { ...tempState.order };
        tempOrder.paymentMethod = paymentMethod;
        let newState = { ...state };
        newState.order = tempOrder;

        return newState;
    }
    if (action.type == ADD_ITEM) {
        let newItemOrder = action.payload;

        let tempState = { ...state };
        let tempOrder = { ...tempState.order };
        if (tempOrder.itemOrders.length > 0) {
            let item = tempOrder.itemOrders[0];

            if (newItemOrder.restaurantId && item.restaurantId != newItemOrder.restaurantId) {
                tempOrder = { ...initialState.order };
            }
        }
        let tempItemOrders = [...tempOrder.itemOrders];
        tempItemOrders.push(newItemOrder);
        tempOrder.itemOrders = tempItemOrders;
        tempOrder.restaurantId = newItemOrder.restaurantId;
        console.log(tempOrder)
        let newState = { ...state };
        newState.order = tempOrder;

        return newState;
    }
    else if (action.type == REMOVE_ITEM) {
        let oldItemOrder = action.payload;
        let tempState = { ...state };
        let tempOrder = { ...tempState.order };
        let tempItemOrders = [...tempOrder.itemOrders];
        let specificOrders = tempItemOrders.filter((item) => item.id == oldItemOrder.id);
        specificOrders.pop();
        tempItemOrders = tempItemOrders.filter((item) => item.id != oldItemOrder.id);
        tempItemOrders.push(...specificOrders);

        tempOrder.itemOrders = tempItemOrders;
        let newState = { ...state };
        newState.order = tempOrder;
        return newState;
    }
    // Order Clean
    else if (action.type == CLEAR_ORDER) {
        let newState = { ...state };
        newState.order = { ...constOrder }
        return newState;
    }
    // Book table
    else if (action.type == BOOK_TABLE) {
        let newState = { ...state };
        let table = action.payload;
        let newOrder = { ...newState.order };
        if (table && table.id && table.restaurantId) {
            newOrder.bookingType = AppConstants.DINE_IN;
            newOrder.tableId = table.id;
            newOrder.restaurantId = table.restaurantId;
        }
        newState.order = newOrder
        return newState;
    }
    // Location Change
    else if (action.type == CHANGE_LOCATION) {
        let newState = { ...state };
        newState.location = action.payload;
        return newState;
    }
    // Logout

    else {
        return state;
    }
}
