import React from 'react';
import './App.css';
import './Firebase';
import Home from './components/home/Home';
import { BrowserRouter } from 'react-router-dom';
import LocationController from './controllers/location_controller';
import { Provider } from "react-redux";
import { appStore, persistor } from '../src/stores/app';
import { Spin } from 'antd';
import { PersistGate } from 'redux-persist/integration/react';
import MainPage from './components/mainPage/MainPage';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Provider store={appStore}>
          <PersistGate loading={<Spin />} persistor={persistor}>
            <MainPage />
          </PersistGate>
        </Provider>
      </div></BrowserRouter>
  );
}

export default App;
