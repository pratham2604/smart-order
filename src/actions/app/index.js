import { ADD_ITEM, REMOVE_ITEM, INITIALIZE_ORDER, CHANGE_BOOKING_TYPE, CHANGE_PAYMENT_METHOD, LOGIN_USER, LOGOUT_USER, CHANGE_LOCATION, CLEAR_ORDER, BOOK_TABLE } from '../../constants/cart';
import { RestaurantItem } from '../../models/restaurant_menu';
import { Restaurant } from '../../models/restaurant';
import PaymentMethod from '../../models/payment_method';


export class CartAction {
    constructor(action, payload) {
        this.action = action;
        this.payload = payload;
    }
}

/**
 * 
 * @param {Restaurant} restaurant 
 */
export function initializeOrder(restaurant) {
    return new CartAction(INITIALIZE_ORDER, restaurant);
}

/**
 * 
 * @param {string} bookingType 
 */
export function changeBookingType(bookingType) {
    return {
        type: CHANGE_BOOKING_TYPE,
        payload: bookingType
    };
}

export function bookTable(table) {
    return {
        type: BOOK_TABLE,
        payload: table
    }
}

/**
 * 
 * @param {string} paymentMethod 
 */
export function changePaymentMethod(paymentMethod) {
    return {
        type: CHANGE_PAYMENT_METHOD,
        payload: paymentMethod
    }
}

/**
 * 
 * @param {RestaurantItem} item 
 */
export function addItem(item) {
    return {
        type: ADD_ITEM,
        payload: item
    };
}


/**
 * 
 * @param {RestaurantItem} item 
 */
export function removeItem(item) {
    return {
        type: REMOVE_ITEM,
        payload: item
    };
}

export function clearOrder() {
    return {
        type: CLEAR_ORDER,
        payload: {}
    };
}

/// 
/// Location
///
export function changeLocation(location) {
    return {
        type: CHANGE_LOCATION,
        payload: location
    }
}

///
/// User
/// 



export function loginUser(user) {
    return {
        type: LOGIN_USER,
        payload: user
    }
}
export function logoutUser() {
    return {
        type: LOGOUT_USER,
        payload: null
    }
}