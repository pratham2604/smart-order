
class LanguageController {

    static getLanguages() {
        return {
            EN: "English",
            HI: "हिन्दी",
            ML: "മലയാളം",
            BA: "বাংলা.",
            PA: "ਪੰਜਾਬੀ",
            OR: "ଓଡ଼ିଆ",
            TA: "தமிழ்",
            TE: "తెలుగు",
            GU: "ગુજરાતી",
            UR: "اُردُو‎",

        };
    }

    static get currentLanguage() {
        // let language = i18n.language;
        // if (language == 'en-US') {
        //     return "EN";
        // }
        // return language;
        return "EN";

    }

}


export default LanguageController;