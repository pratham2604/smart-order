import firebase from 'firebase';
import User from "../models/user";
import AppConstants from '../helpers/app_constants';
import uuid from 'uuid';

class UserController {

    static firestore = firebase.firestore();


    // TODO
    static async loginWithEmail(email, password) {

    }

    // TODO
    static async signupWithEmail(user) {

    }

    // TODO
    static async loginWithGoogle() {

    }

    // TODO
    /**
     * 
     * @param {User} user 
     */
    static async loginWithFacebook(temp) {
        let snapshot = await this.firestore.collection(AppConstants.USER_COLLECTION).where("facebookId", "==", temp.facebookId).get();
        let user;
        if (snapshot.docs.length == 1) {
            user = User.fromJson(snapshot.docs[0].data())
            return user;
        }
        // new user
        user = { ...temp };
        user.id = uuid.v4()
        user = await this.createUser(user);
        return user;
    }

    // TODO
    static async loginWithMobile(result) {
        try {
            const { name, mobileNumber } = result;
            // Checking user with mobile no exists 

            let snapshot = await this.firestore.collection(AppConstants.USER_COLLECTION).where(AppConstants.USER_MOBILE, "==", mobileNumber).get();
            let user;
            if (snapshot.docs.length == 1) {
                user = User.fromJson(snapshot.docs[0].data())
                return user;
            }
            // new user
            user = new User();
            user.mobileNumber = mobileNumber;
            user.name = name;
            user = await this.createUser(user);
            return user;
        } catch (error) {
            console.log(error);
            return null;
        }
    }

    ///
    /// Create User
    ///
    static async createUser(user) {
        let id = uuid.v4();
        let newUser = { ...user };
        newUser.id = id;
        await this.firestore.collection(AppConstants.USER_COLLECTION).doc(id).set(JSON.parse(JSON.stringify(newUser)));
        return newUser;
    }

    // Get New User Details
    static async createtNewUser() {
        let id = uuid.v4();
        let user = new User(null, 'Anonymous');
        let result = await this.createUser(user);
        return result;
    }

    static async getUserDetails(userId) {
        let snapshot = await this.firestore.collection(AppConstants.USER_COLLECTION).doc(userId).get();
        console.log(snapshot.data())
        if (snapshot.data()) {
            return User.fromJson(snapshot.data())
        } else {
            return null;
        }
    }

}

export default UserController;