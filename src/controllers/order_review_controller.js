import OrderReview from "../models/order_review";
import firebase from 'firebase';
import AppConstants from "../helpers/app_constants";
import uuid from "uuid";
import Order from "../models/order";
import RestaurantController from "./restaurant_controller";

export default class OrderReviewController {

    static firestore = firebase.firestore();

    /**
     * 
     * @param {OrderReview} orderReview 
     */
    static async addOrderReview(orderReview) {
        try {
            let newOrder = { ...orderReview };
            newOrder.recordedAt = new Date(Date.now()).toISOString();
            if (!newOrder.id) {
                newOrder.id = uuid.v4();
                await this.firestore.collection(AppConstants.REVIEW_COLLECTION).doc(newOrder.id).set(JSON.parse(JSON.stringify(newOrder)));


            }

            else {
                await this.firestore.collection(AppConstants.REVIEW_COLLECTION).doc(newOrder.id).update(JSON.parse(JSON.stringify(newOrder)));

            }
            let result = await RestaurantController.getRestaurantDetail(orderReview.restaurantId);
            let newResult = { ...result };
            let rating = ((newResult["rating"] * newResult["votesCount"]) + orderReview.rating) / (newResult["votesCount"] + 1);
            let votesCount = newResult.votesCount + 1;
            newResult.rating = rating.toPrecision(1);
            newResult.votesCount = votesCount;
            await RestaurantController.update(newResult);
            return newOrder;
        } catch (error) {
            console.log(error);
            return null;
        }
    }


    static async getOrderReview(userId, orderId) {
        try {

            let snapshot = await this.firestore.collection(AppConstants.REVIEW_COLLECTION).where("customerId", "==", userId).get();
            if (snapshot.docs.length == 0) {
                return {}
            }
            let review = {};
            snapshot.docs.forEach((val) => {
                let json = val.data();
                if (json["orderId"] == orderId) {
                    console.log(json);
                    review = OrderReview.fromJson(json)
                }
            })
            return review;
        } catch (error) {
            console.log(error)
            return {}

        }
    }
}