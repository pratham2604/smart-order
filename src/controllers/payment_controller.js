import firebase from 'firebase';
import AppConstants from '../helpers/app_constants';
import UserController from './user_controller';
import PaymentTransaction from '../models/payment_transaction';
import PaymentMethod from '../models/payment_method';

class PaymentController {

    static firestore = firebase.firestore();

    /**
     * Payment Methods
     */
    static getPaymentMethods() {
        return [
            new PaymentMethod(
                PaymentMethod.CASH,
                "Pay by Cash",
                "Pay at your comfort"
            ),
            // new PaymentMethod(
            //     PaymentMethod.ONLINE,
            //     "Pay by Online",
            //     "Pay easily"
            // )
        ];
    }

    /**
     * Loading payments from collection and
     * order details from order id
     */
    static async getPayments(userId) {
        try {
            let snapshot = await this.firestore.collection(AppConstants.PAYMENTS).where("customerId", "==", userId).get();
            if (snapshot && snapshot.docs && snapshot.docs.length > 0) {
                let docs = snapshot.docs;
                return docs.map((doc) => {
                    return PaymentTransaction.fromJson(doc.data());
                })
            }
            return [];
        } catch (error) {
            console.log(error);
            return [];
        }
    }

    /**
     * 
     * @param {PaymentTransaction} paymentTransaction 
     */
    static async savePayment(paymentTransaction) {
        try {
            await this.firestore.collection(AppConstants.PAYMENTS).doc(paymentTransaction.id).set(JSON.parse(JSON.stringify(paymentTransaction)))
        } catch (error) {
            console.log(error)
        }
    }

}

export default PaymentController;