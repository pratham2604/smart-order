import Axios from 'axios';
import AppConstants from '../helpers/app_constants';


class OTPController {

  // OTP Request base url
  static baseUrl = "https://control.msg91.com/api/";

  // Auth Key
  static API_KEY = "authkey=210655AE9Q8a7YZl5ad7230c";

  static async sendOTP(mobileNumber) {

    // OTP Request base url
    let baseUrl = "https://control.msg91.com/api/";

    // Auth Key
    let API_KEY = "authkey=295797ABXcn9ci4IWQ5d8b0c1c";
    try {
      let url =
        baseUrl + `sendotp.php?mobile=${mobileNumber}&otp_length=6&${API_KEY}`;
      let response = await Axios.post(AppConstants.getURL(url));
      console.log(response.data);
      var json = response.data;
      if (json["type"] == "success") {
        return null;
      }
      else {
        return json["message"];
      }
    } catch (error) {
      return `${error}`;
    }
  }

  static async verifyOTP(mobileNumber, otp) {

    // OTP Request base url
    let baseUrl = "https://control.msg91.com/api/";

    // Auth Key
    let API_KEY = "authkey=295797ABXcn9ci4IWQ5d8b0c1c";
    try {
      let url =
        baseUrl + `verifyRequestOTP.php?mobile=${mobileNumber}&otp=${otp}&${API_KEY}&country=0`;
      let response = await Axios.post(AppConstants.getURL(url));
      console.log(response.data);
      var json = response.data;
      if (json["type"] == "success") {
        return null;
      }
      else {
        return json["message"];
      }
    } catch (error) {
      return `${error}`;
    }
  }


}

export default OTPController;