import firebase from 'firebase';
import UserController from '../controllers/user_controller';
import AppConstants from '../helpers/app_constants';
import Order from '../models/order';
import uuid from 'uuid';
import User from '../models/user';
import PaymentMethod from '../models/payment_method';
import PaymentTransaction from '../models/payment_transaction';
import PaymentController from './payment_controller';
import TableController from './table_controller';
import moment from 'moment';
const DATE_CONFIG = 'DD MM YYYY';
const MONTH_CONFIG = 'MM YYYY';
const BILL_ORDER_CONFIG = DATE_CONFIG;

class OrderController {

    static firestore = firebase.firestore();
    /**
     * Load Orders
     */
    static getOrders(userId, onLoaded) {

        return this.firestore.collection(AppConstants.ORDERS).where("customerId", "==", userId).onSnapshot((snapshot) => {
            let result = [];
            if (snapshot && snapshot.docs) {
                result = snapshot.docs.map((doc) => Order.fromJson(doc.data())).sort((b, a) => a.recordedAt.toString().localeCompare(b.recordedAt.toString()));
            }
            onLoaded(result);
        });

    }

    static async loadOrderDetails(id) {
        try {
            let snapshot = await this.firestore.collection(AppConstants.ORDER_COLLECTION).doc(id).get();
            if (snapshot.data()) {
                return Order.fromJson(snapshot.data());
            }
        } catch (error) {
            return null
        }
    }

    /**
     * 
     * @param {Order} orderDetails 
     */
    static canConfirmOrder(orderDetails) {
        let checkItemOrders = Array.isArray(orderDetails.itemOrders) && orderDetails.itemOrders.length > 0;
        let checkBookingtype = orderDetails.bookingType != undefined;
        let checkPaymentMethod = orderDetails.paymentMethod != undefined;
        return checkItemOrders && checkPaymentMethod && checkBookingtype;
    }

    /**
     * @param {User} user
     * @param {Order} order 
     * @param {string} paymentId 
     */
    static async confirmOrder(user, order, paymentId) {
        let newOrder = { ...order };
        newOrder.id = uuid.v4();
        // Save payment transaction
        let uid = uuid.v4();
        let paymentTransaction = new PaymentTransaction(uid, paymentId ? user.id : null, newOrder.id, paymentId || null, newOrder.restaurantId, Order.totalCost(order), new Date(Date.now()).toISOString(), paymentId ? "Online" : "Cash");
        await PaymentController.savePayment(paymentTransaction)
        //
        newOrder.recordedAt = Date.now();
        newOrder.statusCode = AppConstants.NOT_CONFIRMED;
        newOrder.customerId = user.id
        newOrder.totalCost = Order.totalCost(newOrder)
        console.log(newOrder);

        const totalOrders = [];
        const snapshot = await this.firestore.collection(AppConstants.ORDER_COLLECTION).where("restaurantId", "==", newOrder.restaurantId).get();
        snapshot.docs.forEach((val) => {
            if (val && val.data()) {
                totalOrders.push(val.data());
            }
        })
        const today = moment().format(BILL_ORDER_CONFIG);
        const todaysOrders = totalOrders.filter(order => moment(order.recordedAt).format(BILL_ORDER_CONFIG) === today);
        newOrder.number = (todaysOrders.length + 1);

        this.firestore.collection(AppConstants.ORDER_COLLECTION).doc(newOrder.id).set(JSON.parse(JSON.stringify(newOrder)));

    }

    static async makeCancelRequest(orderId, customerId, restaurantId, message) {
        let cancelRequest = {
            id: uuid.v4(),
            orderId: orderId,
            customerId: customerId,
            restaurantId: restaurantId,
            message: message,
            recordedAt: new Date(Date.now()).toISOString()
        };
        await this.firestore.collection(AppConstants.CANCEL_REQUESTS_COLLECTION).doc(cancelRequest.id).set(JSON.parse(JSON.stringify(cancelRequest)));

    }

}

export default OrderController;