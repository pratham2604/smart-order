import axios from 'axios';
import AppLocation from '../models/app_location';
import AppConstants from '../helpers/app_constants';

const API_KEY = "AIzaSyC2tm6j8esbGBvpDeOzAO88YICoae5JgtE";
const BASE_URL = `https://maps.googleapis.com/maps/api/place/autocomplete/json?types=geocode&key=${API_KEY}`;
const PLACE_URL = "https://maps.googleapis.com/maps/api/place/details/json?key=" + API_KEY;
class LocationController {

    /**
     * 
     * @param {string} query 
     */
    static async getSuggestions(query) {
        try {
            let response = await axios.get(AppConstants.getURL(BASE_URL
                + "&input=" + query));
            let body = response.data;
            if (body["predictions"]) {
                return body["predictions"].map((val) => AppLocation.fromJson(val));
            }
            return [];

        } catch (error) {
            return [];
        }
    }

    /**
     * 
     * @param {string} placeId 
     */
    static async getLocationDetails(placeId) {
        try {
            let response = await axios.get(AppConstants.getURL(PLACE_URL + `&place_id=${placeId}`));
            let body = response.data;

            if (!body["result"]) {
                return null;
            }
            let result = body["result"];
            let location = result["geometry"]["location"];
            return new AppLocation(
                result["name"],
                result["formatted_address"],
                result["place_id"],
                location["lat"],
                location["lng"]
            );

        } catch (error) {
            console.log(error)
            return null;
        }
    }

    /**
     * Current Location 
     */
    // static async getCurrentLocation() {
    //     navigator.geolocation.getCurrentPosition((position) => {
    //     })
    // }

}

export default LocationController;