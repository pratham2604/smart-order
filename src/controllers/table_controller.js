import RestaurantTable from "../models/restaurant_table";
import { firestore } from "firebase";
import AppConstants from "../helpers/app_constants";

const crypto = require('crypto');

export default class TableController {

    static async getTableDetails(code, ) {
        try {
            let snapshot = await firestore().collection(AppConstants.RESTAURANT_TABLE_COLLECTION).doc(code).get();
            console.log(snapshot.data())
            if (snapshot.data()) {
                return RestaurantTable.fromJson(snapshot.data())
            }
            else {
                return null;
            }
        } catch (error) {
            console.log(error);
            return null;
        }
    }



    static getTableId() {
        var id = crypto.randomBytes(3).toString('hex').toUpperCase();
        return id;
    }
}