import firebase from 'firebase';
import AppConstants from "../helpers/app_constants";
import Restaurant, { RestaurantFilter } from '../models/restaurant';
import { RestaurantMenu, Cuisine } from '../models/restaurant_menu';

class RestaurantController {

    static firestore = firebase.firestore();

    /**
     * Get Restaurant Filter
     */
    static async getRestaurantFilter() {
        let restaurantFilter = new RestaurantFilter([], null, []);
        let snapshot = await this.firestore.collection(AppConstants.CUISINES).get();
        if (snapshot && snapshot.docs.length > 0) {
            let docs = snapshot.docs;
            restaurantFilter.cuisines = docs.map((val) => Cuisine.fromJson(val.data()));
        }
        restaurantFilter.keys = [
            { name: "Rating", id: "rating" },
            { name: "Price", id: "averageCostForTwo" },
        ];
        return restaurantFilter;
    }

    static async getCuisines() {
        let cuisines = [];
        let snapshot = await this.firestore.collection(AppConstants.CUISINES).get();
        if (snapshot && snapshot.docs.length > 0) {
            let docs = snapshot.docs;
            cuisines = docs.map((val) => Cuisine.fromJson(val.data()));
        }
        return cuisines;
    }

    /**
     * Get list of restaurants
     */
    static async getRestaurants() {
        let snapshot = await this.firestore.collection(AppConstants.RESTAURANTS).get();
        let restaurants = [];
        snapshot.docs.forEach((val) => {
            if (val && val.data()) {
                restaurants.push(Restaurant.fromJson(val.data()));
            }
        })
        return restaurants;
    }

    /**
     * Get Details of restaurants
     */
    static async getRestaurantDetail(restaurantId) {
        try {
            let snapshot = await this.firestore.collection(AppConstants.RESTAURANTS).doc(restaurantId).get();
            let data = snapshot.data();
            if (data) {
                let restaurant = Restaurant.fromJson(data);
                return restaurant;
            }
            return null;
        } catch (error) {
            console.log(error);
            return null;
        }
    }

    /**
    * Get Details of restaurants
    * @param {Restaurant} restaurant 
    */
    static async update(restaurant) {
        try {
            await this.firestore.collection(AppConstants.RESTAURANTS).doc(restaurant.id).update(JSON.parse(JSON.stringify(restaurant)));

        } catch (error) {
            console.log(error);
            return null;
        }
    }

    static async getRestaurantMenu(restaurantId) {
        try {
            let snapshot = await this.firestore.collection(AppConstants.RESTAURANT_MENU).doc(restaurantId).get();
            let data = snapshot.data();
            console.log("Id", restaurantId, data)
            if (data) {
                let restaurantMenu = RestaurantMenu.fromJson(data);
                return restaurantMenu;
            }
        } catch (error) {

        }
    }

    /**
     * Search restaurants by query
     * 
     * @param {string} query 
     */
    static async searchRestaurants(query) {
        try {
            let snapshot = await this.firestore.collection(AppConstants.RESTAURANTS).orderBy("name").startAt(query).get();
            let restaurants = [];

            snapshot.docs.forEach((val) => {
                if (val && val.data()) {
                    restaurants.push(Restaurant.fromJson(val.data()));
                }
            })
            return restaurants;

        } catch (error) {
            console.log(error);
            return [];
        }
    }

    /**
     * Search restaurants by cuisine
     * 
     * @param {string} query 
     */
    static async getRestaurantsByCuisine(cuisine) {
        try {
            let snapshot = await this.firestore.collection(AppConstants.RESTAURANTS).where("cuisineIds", "array-contains", cuisine).get();
            let restaurants = [];
            snapshot.docs.forEach((val) => {
                if (val && val.data()) {
                    restaurants.push(Restaurant.fromJson(val.data()));
                }
            })
            return restaurants;

        } catch (error) {
            console.log(error);
            return [];
        }
    }

    /**
     * Get Link for restaurant page
     */
    static getRestaurantLink(id) {
        return `/restaurant?id=${id}`;
    }

    /**
     * Get Food Items locally
     */
    static getFoodItems() {
        return [
            {
                "name": "Pizza",
                "image": "pizza.jpg",
            },
            {
                "name": "Cookies",
                "image": "cookies.jpg",
            },
            {
                "name": "S'mores",
                "image": "smores.jpg",
            },
            {
                "name": "HotDog",
                "image": "hotdog.jpg",
            },
            {
                "name": "Cheesecake",
                "image": "cheesecake.jpg",
            }
        ].map((val) => {
            let item = { ...val };
            item.image = "/images/foodItems/" + item.image;
            return item;
        });
    }

    /**
 * Get Food Items locally
 */
    static getCategories() {
        return [
            {
                "name": "Italian",
                "image": "italian.png",
            },
            {
                "name": "American",
                "image": "american.png",
            },
            {
                "name": "Thai",
                "image": "thai.png",
            },

        ].map((val) => {
            let item = { ...val };
            item.image = "/images/categories/" + item.image;
            return item;
        });
    }


}

export default RestaurantController;