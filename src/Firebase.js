import app from 'firebase/app';

var firebaseConfig = {
    apiKey: "AIzaSyC2tm6j8esbGBvpDeOzAO88YICoae5JgtE",
    authDomain: "smart-ordr.firebaseapp.com",
    databaseURL: "https://smart-ordr.firebaseio.com",
    projectId: "smart-ordr",
    storageBucket: "smart-ordr.appspot.com",
    messagingSenderId: "894064438622",
    appId: "1:894064438622:web:27529772f0ada54c21f3cf",
    measurementId: "G-W8Z6KGY9RN"
};
const firebaseApp = app.initializeApp(firebaseConfig);
export default firebaseApp;