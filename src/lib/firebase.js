import * as FirebaseModule from 'firebase';
import firebaseConfig from '../constants/firebase';



let firebaseInitialized = false;

FirebaseModule.initializeApp(firebaseConfig);

firebaseInitialized = true;
export const FirebaseRef = firebaseInitialized ? FirebaseModule.database().ref() : null;
export const Firebase = firebaseInitialized ? FirebaseModule : null;
